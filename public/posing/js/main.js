$(function(){    

	$.fn.filterByData = function(prop, val) {
        return this.filter(
            function() { return $(this).data(prop)==val; }
        );
    };

    var carousel = $('.owlCarousel').owlCarousel({
			            singleItem: true,
			            slideSpeed: 700,
			            mouseDrag:false,
			            pagination:false,
			            touchDrag:false,
			            addClassActive:true,
			            autoHeight:true
			        });


    var App = function(settings){

		this.config = {
			point: $('.nav-pagination .point'),
			button: $('[data-btn-event]'),
			hashTitle: $('[data-title]'), 
			social: $('[data-provider]'), 
			image: $('[data-image]'), 
			textContainer: $('.d-text'), 
			onLoad:false,				
			afterChange:false				
		};

		$.extend( this.config, settings ); 	
    };

   	App.prototype = {
        
        init:function(settings){

	        this.setup();
	        this.onLoad();

        },

        setup: function(){

         	var self = this;
         		
	     		self.setVars();
				self.paginationControls();
				self.buttonControls();
				self.updateOnResize();

				self.share();

        	 				      	 				
	         	
        }, 

        setVars: function(){

        	this.currentItem = 0;   
		    this.shareProviders = {

		        vk: function(data) {
		            url = 'http://vk.com/share.php?';
		            url += 'url=' + encodeURIComponent(data.url);
		            url += '&title=' + encodeURIComponent(data.title);
		            url += '&description=' + encodeURIComponent(data.description);
		            url += '&image=' + encodeURIComponent(data.image);
		            url += '&noparse=true';
		            this.popup(url);
		        },
		        od: function(data) {
		            url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl='+encodeURIComponent(data.url);
		            this.popup(url);
		        },
		        fb: function(data) {
		 	        url  = 'http://www.facebook.com/sharer.php?s=100';
			        url += '&p[url]='+ encodeURIComponent(data.url);
		            this.popup(url);
		        },
		        tw: function(data) {
			        url  = 'http://twitter.com/share?';
			        url += 'text='      + encodeURIComponent(data.title);
			        url += '&url='      + encodeURIComponent(window.location.href);
 		            this.popup(url);
		        },
		        ml: function(data) {
		            url = 'http://connect.mail.ru/share?';
		            url += 'url=' + encodeURIComponent(data.pulr);
		            url += '&title=' + encodeURIComponent(data.title);
		            url += '&description=' + encodeURIComponent(data.description);
		            url += '&imageurl=' + encodeURIComponent(data.image);
		            this.popup(url);
		        },
		        gl: function(data) {
		            url = 'https://plus.google.com/share?hl=ru';
		            url += '&url=' + encodeURIComponent(data.url);
		            url += '&title=' + encodeURIComponent(data.title);
		            this.popup(url);
		        },

		        popup: function(url) {
		            window.open(url, '', 'toolbar=0,status=0,width=554,height=436');
		        }
		    };

        },

        buttonControls: function(current){

    		var self = this;

	        	self.config.button.on('mouseup', function(){

			        if($(this).data('btnEvent') == 'next'){
			        	 
			            self.config.point.eq(self.currentItem).next().trigger('mouseup');

			        }else if($(this).data('btnEvent') == 'prev'){
 			            
 			            self.config.point.eq(self.currentItem).last().prev().trigger('mouseup');
			        
			        }

	        	});

        },

        paginationControls:function(){

        	var self = this;

	         	self.config.point.on('mouseup', function(){
	         		self.upDate($(this).index());
	         	});

        },  

        getTitleToArray: function(){

            var title = this.config.hashTitle,
                arr = [];

            title.each(function(){
                var text = $(this).data('title');
                    if(!text){
                        text = 'slide'+$(this).data('index');
                    }
                arr.push(text);
            });

            return arr;
        },        

        getDescriptionToArray: function(){

            var title = this.config.textContainer,
                arr = [];

            title.each(function(){
                var text = $(this).text().replace(/[\r\n]/g, '').slice(0,200)+'...';
                    if(!text){
                        text = '';
                    }
                arr.push(text);
            });

            return arr;
        },  

        getHashToArray: function(){

            var titles = this.getTitleToArray(),
                arr = [];

 				$.each(titles, function(i){
 					arr.push(titles[i].replace(/\s/g,''))
 				});

 				return arr;
        },

        setHash: function(ind){

            var hashList = this.getHashToArray();

            window.location.hash = hashList[ind];

        },
        setLineWidth: function(num){

            var el = this.config.point.eq(num),
            	parent = el.parent()
                line = parent.parent().find('.line')
                position = Math.ceil(el.position().left);
                
            line.css('width', position);


        },         
        loadCurrentSlide: function(hash){

            var hashList = this.getHashToArray();

            if(hash){

                var number = hashList.indexOf(hash) || 0;

                	this.goTo(number, 'jumpTo');
	                this.config.point.eq(number).trigger('mouseup');
            }else{
                	this.goTo(0, 'jumpTo');
	                this.config.point.eq(0).trigger('mouseup');            	
            }       
        },   
        goTo: function(i, type){

			carousel.trigger('owl.'+type, i);

        },      
        upDate: function(el){

            this.currentItem = el;
            this.updateClasses(el);
            this.goTo(el, 'goTo');
            this.setHash(el);
            this.updateButtons(el);
            this.setLineWidth(el);    
			
			  


            this.onChange();
       
        },

        updateClasses: function(el){

            this.config.point.eq(el).nextAll().removeClass('current');
            this.config.point.eq(el).addClass('current').prevAll().addClass('current');

        },

        updateButtons: function(i){

            var pointsLength = this.config.point.length;

	            this.config.button.removeClass('disabled')

	            if(i == pointsLength - 1){
	                this.config.button.filterByData('btn-event', 'next').addClass('disabled');
	            }

	            if(i == 0){
	                this.config.button.filterByData('btn-event', 'prev').addClass('disabled');
	            } 

        },

        updateOnResize: function(){
        		
        		var self = this;

	         	$(window).on('resize', function(){
	         		var current = self.config.point.eq(self.currentItem).index();
 		         	self.setLineWidth(current);
	         	});
	
        },

        onLoad : function(){

            var self = this,
            	hash = window.location.hash.replace(/#/g,'');

	            if (typeof self.config.onLoad === "function") {
	                self.config.onLoad.apply(this);
	            }

				self.loadCurrentSlide(hash); 
        },

        onChange: function(){
            
            var self = this;

	            if (typeof self.config.onChange === "function") {
	                self.config.onChange.apply(this);
	            }
        },

        share: function(){

        	var self = this,
        		data = self.createUrl();


        	self.config.social.on('click', function(e){
        		e.preventDefault();

        		self.shareProviders[$(this).data('provider')](self.createUrl())

        	})

 
        },
        createUrl: function(){


			var title = document.title,
 				description = this.getTitleToArray()[this.currentItem],
	    		image = this.config.image.eq(this.currentItem).find('img').attr('src')


			var locationArray = window.location.pathname.split('/'),
				d, data;

				locationArray.pop()
			
			var part = locationArray[1];

			if(typeof part !== 'undefined'){
				d = window.location.origin +'/'+ part;
	 		}else{
				d = window.location.origin;
	 		};
 		

	 		data = {
	 			title: title,
	 			image: d +'/'+ image,
	 			description: description,
	 			pulr: d,
	 			url: d + '/share.php?image='+ d +'/'+ image+'&title='+encodeURIComponent(title)+'&description='+encodeURIComponent(description)+'&redirect='+encodeURIComponent(window.location.href) 
	 		}

	 		return data;
 
        }


    };

	var page = new App();
		page.init();
 
})