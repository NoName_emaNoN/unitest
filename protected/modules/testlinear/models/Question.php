<?php

/**
 * This is the model class for table "{{testlinear_question}}".
 *
 * The followings are the available columns in table '{{testlinear_question}}':
 * @property integer $id
 * @property string $text
 * @property integer $status
 * @property string $image
 * @property integer $test_id
 *
 * The followings are the available model relations:
 * @property Answer[] $answers
 * @property integer $answersCount
 * @property Test $test
 *
 * @method Question active()
 */
class Question extends yupe\models\YModel
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{testlinear_question}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status, test_id', 'numerical', 'integerOnly' => true),
            array('text', 'length', 'max' => 1000),
            array('text', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, text, status', 'safe', 'on' => 'search'),
        );
    }

    public function behaviors()
    {
        $module = Yii::app()->getModule('testlinear');

        return [
            'imageUpload' => [
                'class'          => 'yupe\components\behaviors\ImageUploadBehavior',
                'scenarios'      => ['insert', 'update'],
                'attributeName'  => 'image',
                'minSize'        => $module->minSize,
                'maxSize'        => $module->maxSize,
                'types'          => $module->allowedExtensions,
                'uploadPath'     => $module->uploadPath,
                'resizeOnUpload' => false,
            ],
        ];
    }

    public function scopes()
    {
        return [
            'active' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params'    => [':status' => self::STATUS_ACTIVE],
            ],
        ];
    }

    public function test($test_id)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'condition' => $this->tableAlias . '.test_id = :test_id',
                'params'    => [':test_id' => $test_id]
            ]
        );

        return $this;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'answers'      => array(self::HAS_MANY, 'Answer', 'question_id'),
            'answersCount' => array(self::STAT, 'Answer', 'question_id'),
            'test'         => [self::BELONGS_TO, 'Test', 'test_id'],
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'      => 'ID',
            'text'    => 'Текст вопроса',
            'status'  => 'Статус',
            'image'   => 'Иозбражение',
            'test_id' => 'Тест',
            'answers' => 'Ответы',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.test_id', $this->test_id);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Question the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getStatusList()
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Скрыт',
            self::STATUS_ACTIVE     => 'Активен',
        ];
    }

    public function getStatus()
    {
        return $this->getStatusList()[$this->status];
    }
}
