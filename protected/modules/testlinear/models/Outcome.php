<?php

/**
 * This is the model class for table "{{testlinear_outcome}}".
 *
 * The followings are the available columns in table '{{testlinear_outcome}}':
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property string $css_class
 * @property string $image
 * @property integer $min_right_answers
 * @property integer $max_right_answers
 * @property integer $test_id
 *
 * @property Test $test
 */
class Outcome extends yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{testlinear_outcome}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, min_right_answers, max_right_answers, test_id', 'required'),
            ['name, css_class, text', 'filter', 'filter' => 'trim'],
            ['name, css_class', 'filter', 'filter' => array(new CHtmlPurifier(), 'purify')],
            array('name, css_class', 'length', 'max' => 255),
            ['min_right_answers, max_right_answers, test_id', 'numerical', 'integerOnly' => true],
            array('text', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, text, css_class, min_right_answers, max_right_answers, test_id', 'safe', 'on' => 'search'),
        );
    }

    public function test($test_id)
    {
        $this->getDbCriteria()->mergeWith(
            [
                'condition' => $this->tableAlias . '.test_id = :test_id',
                'params'    => [':test_id' => $test_id],
            ]
        );

        return $this;
    }

    public function behaviors()
    {
        $module = Yii::app()->getModule('testlinear');

        return [
            'imageUpload' => [
                'class'          => 'yupe\components\behaviors\ImageUploadBehavior',
                'scenarios'      => ['create', 'insert', 'update'],
                'attributeName'  => 'image',
                'minSize'        => $module->minSize,
                'maxSize'        => $module->maxSize,
                'types'          => $module->allowedExtensions,
                'uploadPath'     => $module->uploadPath,
                'resizeOnUpload' => false,
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'test' => [self::BELONGS_TO, 'Test', 'test_id'],
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                => 'ID',
            'name'              => 'Название',
            'text'              => 'Текст',
            'css_class'         => 'CSS Class',
            'image'             => 'Иозбражение',
            'min_right_answers' => 'Минимальное кол-во правильных ответов (>=)',
            'max_right_answers' => 'Максимальное кол-во правильных ответов (<=)',
            'test_id'           => 'Тест',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.name', $this->name, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.css_class', $this->css_class, true);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.min_right_answers', $this->min_right_answers);
        $criteria->compare($this->tableAlias . '.max_right_answers', $this->max_right_answers);
        $criteria->compare($this->tableAlias . '.test_id', $this->test_id);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Outcome the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
