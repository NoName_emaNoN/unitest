<?php

/**
 * This is the model class for table "{{testlinear_user_session}}".
 *
 * The followings are the available columns in table '{{testlinear_user_session}}':
 * @property integer $id
 * @property string $user_id
 * @property integer $status
 * @property string $close_date
 * @property integer $test_id
 *
 * The followings are the available model relations:
 * @property UserSessionAnswer[] $answers
 * @property integer $answerCount
 * @property Test $test
 */
class UserSession extends yupe\models\YModel
{
    const STATUS_OPENED = 0;
    const STATUS_CLOSED = 1;
    const STATUS_ABORTED = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{testlinear_user_session}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            ['test_id', 'required'],
            array('status, test_id', 'numerical', 'integerOnly' => true),
            array('user_id', 'length', 'max' => 255),
            ['close_date', 'default', 'setOnEmpty' => true, 'value' => null],
            array('user_id, status, close_date', 'unsafe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, status, close_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'answers'     => array(self::HAS_MANY, 'UserSessionAnswer', 'session_id'),
            'answerCount' => [self::STAT, 'UserSessionAnswer', 'session_id'],
            'test'        => [self::BELONGS_TO, 'Test', 'test_id'],
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'         => 'ID',
            'user_id'    => 'Пользователь',
            'status'     => 'Статус',
            'close_date' => 'Дата прохождения теста',
            'test'       => 'Тест',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.user_id', $this->user_id, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.close_date', $this->close_date, true);
        $criteria->compare($this->tableAlias . '.test_id', $this->test_id);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserSession the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->status = self::STATUS_OPENED;
            $this->user_id = Yii::app()->user->getUid();
        }

        return parent::beforeSave();
    }

    public function close()
    {
        $this->status = self::STATUS_CLOSED;
        $this->close_date = new CDbExpression('NOW()');
    }

    public function getStatusList()
    {
        return array(
            self::STATUS_OPENED  => 'Открыто',
            self::STATUS_CLOSED  => 'Закрыто',
            self::STATUS_ABORTED => 'Прервано',
        );
    }

    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : '-';
    }

    /**
     * @return Outcome
     */
    public function getOutcome()
    {
        $rightAnswersCount = $this->getRightAnswersCount();

        $criteria = new CDbCriteria();

        $criteria->addCondition('t.min_right_answers <= ' . $rightAnswersCount);
        $criteria->addCondition('t.max_right_answers >= ' . $rightAnswersCount);

        $outcome = Outcome::model()->test($this->test_id)->find($criteria);

        if (!$outcome) {
            throw new CException('Не удалось найти исход для Вашего кол-ва правильных ответов!');
        }

        return $outcome;
    }

    public function getUrl($absolute = false)
    {
        return call_user_func([Yii::app(), $absolute ? 'createAbsoluteUrl' : 'createUrl'], '/testlinear/question/result', ['session' => $this->id, '#' => 'test']);
    }

    public function getRightAnswersCount()
    {
        $count = 0;

        foreach ($this->answers as $answer) {
            $count += (int)$answer->answer->is_right_answer;
        }

        return $count;
    }
}
