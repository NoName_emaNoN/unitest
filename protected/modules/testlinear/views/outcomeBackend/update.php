<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testlinear')->getCategory() => array(),
        Yii::t('testlinear', 'Исходы') => array('/testlinear/outcomeBackend/index'),
        $model->name => array('/testlinear/outcomeBackend/view', 'id' => $model->id),
        Yii::t('testlinear', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('testlinear', 'Исходы - редактирование');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testlinear', 'Управление исходами'), 'url' => array('/testlinear/outcomeBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testlinear', 'Добавить исход'), 'url' => array('/testlinear/outcomeBackend/create')),
        array('label' => Yii::t('testlinear', 'Исход') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('testlinear', 'Редактирование исхода'), 'url' => array(
            '/testlinear/outcomeBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('testlinear', 'Просмотреть исход'), 'url' => array(
            '/testlinear/outcomeBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('testlinear', 'Удалить исход'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/testlinear/outcomeBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('testlinear', 'Вы уверены, что хотите удалить исход?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testlinear', 'Редактирование') . ' ' . Yii::t('testlinear', 'исхода'); ?>        <br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>