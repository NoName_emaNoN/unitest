<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testlinear')->getCategory() => array(),
        Yii::t('testlinear', 'Исходы') => array('/testlinear/outcomeBackend/index'),
        Yii::t('testlinear', 'Добавление'),
    );

    $this->pageTitle = Yii::t('testlinear', 'Исходы - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testlinear', 'Управление исходами'), 'url' => array('/testlinear/outcomeBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testlinear', 'Добавить исход'), 'url' => array('/testlinear/outcomeBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testlinear', 'Исходы'); ?>
        <small><?php echo Yii::t('testlinear', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>