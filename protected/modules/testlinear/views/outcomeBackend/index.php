<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
    Yii::app()->getModule('testlinear')->getCategory() => array(),
    Yii::t('testlinear', 'Исходы')                     => array('/testlinear/outcomeBackend/index'),
    Yii::t('testlinear', 'Управление'),
);

$this->pageTitle = Yii::t('testlinear', 'Исходы - управление');

$this->menu = array(
    array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testlinear', 'Управление исходами'), 'url' => array('/testlinear/outcomeBackend/index')),
    array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testlinear', 'Добавить исход'), 'url' => array('/testlinear/outcomeBackend/create')),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testlinear', 'Исходы'); ?>
        <small><?php echo Yii::t('testlinear', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('testlinear', 'Поиск исходов'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('outcome-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', array('model' => $model));
    ?>
</div>

<br/>

<p> <?php echo Yii::t('testlinear', 'В данном разделе представлены средства управления исходами'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    array(
        'id'           => 'question-outcome-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            [
                'name'        => 'id',
                'htmlOptions' => ['width' => '60'],
            ],
            [
                'name'   => 'image',
                'type'   => 'raw',
                'value'  => 'CHtml::image($data->getImageUrl(75, 75), "", array("width" => 75, "height" => 75))',
                'filter' => false
            ],
            'name',
            'text',
//            'css_class',
            'min_right_answers',
            'max_right_answers',
            array(
                'class' => 'yupe\widgets\CustomButtonColumn',
            ),
        ),
    )
); ?>
