<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 *
 *   @var $model Test
 *   @var $form TbActiveForm
 *   @var $this TestBackendController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', array(
        'id'                     => 'test-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    )
);
?>

<div class="alert alert-info">
    <?php echo Yii::t('testlinear', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('testlinear', 'обязательны.'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup($model, 'name', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('name'), 'data-content' => $model->getAttributeDescription('name'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textAreaGroup($model, 'text', array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'rows' => 6, 'cols' => 50, 'data-original-title' => $model->getAttributeLabel('text'), 'data-content' => $model->getAttributeDescription('text'))))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup(
                $model,
                'status',
                array(
                    'widgetOptions' => array(
                        'data'        => $model->getStatusList(),
                        'htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('status'), 'data-content' => $model->getAttributeDescription('status'))
                    )
                )
            ); ?>
        </div>
    </div>
    <div class='row'>
        <div class="col-sm-7">
            <?php
            echo CHtml::image(
                !$model->isNewRecord && $model->image ? $model->getImageUrl() : '#',
                '',
                [
                    'class' => 'preview-image',
                    'style' => !$model->isNewRecord && $model->image ? '' : 'display:none'
                ]
            ); ?>
            <?php echo $form->fileFieldGroup($model, 'image'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup(
                $model,
                'theme',
                array(
                    'widgetOptions' => array(
                        'data'        => array_combine(Yii::app()->themeManager->themeNames, Yii::app()->themeManager->themeNames),
                        'htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('theme'), 'data-content' => $model->getAttributeDescription('theme'))
                    )
                )
            ); ?>
        </div>
    </div>


<?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context'    => 'primary',
            'label'      => Yii::t('testlinear', 'Сохранить тест и продолжить'),
        )
    ); ?>

    <?php
    $this->widget(
        'bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'htmlOptions'=> array('name' => 'submit-type', 'value' => 'index'),
            'label'      => Yii::t('testlinear', 'Сохранить тест и закрыть'),
        )
    ); ?>

<?php $this->endWidget(); ?>