<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testlinear')->getCategory() => array(),
        Yii::t('testlinear', 'Тесты') => array('/testlinear/testBackend/index'),
        Yii::t('testlinear', 'Добавление'),
    );

    $this->pageTitle = Yii::t('testlinear', 'Тесты - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testlinear', 'Управление тестами'), 'url' => array('/testlinear/testBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testlinear', 'Добавить тест'), 'url' => array('/testlinear/testBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testlinear', 'Тесты'); ?>
        <small><?php echo Yii::t('testlinear', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>