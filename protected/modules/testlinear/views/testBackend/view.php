<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testlinear')->getCategory() => array(),
        Yii::t('testlinear', 'Тесты') => array('/testlinear/testBackend/index'),
        $model->name,
    );

    $this->pageTitle = Yii::t('testlinear', 'Тесты - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testlinear', 'Управление тестами'), 'url' => array('/testlinear/testBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testlinear', 'Добавить тест'), 'url' => array('/testlinear/testBackend/create')),
        array('label' => Yii::t('testlinear', 'Тест') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('testlinear', 'Редактирование теста'), 'url' => array(
            '/testlinear/testBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('testlinear', 'Просмотреть тест'), 'url' => array(
            '/testlinear/testBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('testlinear', 'Удалить тест'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/testlinear/testBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('testlinear', 'Вы уверены, что хотите удалить тест?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testlinear', 'Просмотр') . ' ' . Yii::t('testlinear', 'теста'); ?>        <br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'name',
        'text',
        'status',
        'image',
),
)); ?>
