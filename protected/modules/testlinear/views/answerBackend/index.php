<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
    Yii::app()->getModule('testlinear')->getCategory() => array(),
    Yii::t('testlinear', 'Ответы')                     => array('/testlinear/answerBackend/index'),
    Yii::t('testlinear', 'Управление'),
);

$this->pageTitle = Yii::t('testlinear', 'Ответы - управление');

$this->menu = array(
    array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testlinear', 'Управление ответами'), 'url' => array('/testlinear/answerBackend/index')),
    array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testlinear', 'Добавить ответ'), 'url' => array('/testlinear/answerBackend/create')),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testlinear', 'Ответы'); ?>
        <small><?php echo Yii::t('testlinear', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('testlinear', 'Поиск ответов'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('answer-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', array('model' => $model));
    ?>
</div>

<br/>

<p> <?php echo Yii::t('testlinear', 'В данном разделе представлены средства управления ответами'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    array(
        'id'           => 'question-answer-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            [
                'name'        => 'id',
                'htmlOptions' => ['width' => '60'],
            ],
            array(
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'editable' => array(
                    'url'    => $this->createUrl('inline'),
                    'type'   => 'select',
                    'title'  => 'Выберите вопрос',
                    'source' => CHtml::listData(Question::model()->findAll(), 'id', 'text'),
                    'params' => array(
                        Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken
                    )
                ),
                'name'     => 'question_id',
                'type'     => 'raw',
                'value'    => '$data->question->text',
                'filter'   => CHtml::activeDropDownList(
                    $model,
                    'question_id',
                    CHtml::listData(Question::model()->findAll(), 'id', 'text'),
                    array('class' => 'form-control', 'empty' => '')
                ),
            ),
            'text',
            [
                'class'  => '\yupe\widgets\ToggleColumn',
                'name'   => 'is_right_answer',
                'filter' => Yii::app()->getModule('yupe')->getChoice(),
            ],
            array(
                'class' => 'yupe\widgets\CustomButtonColumn',
            ),
        ),
    )
); ?>
