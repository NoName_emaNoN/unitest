<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testlinear')->getCategory() => array(),
        Yii::t('testlinear', 'Ответы') => array('/testlinear/answerBackend/index'),
        Yii::t('testlinear', 'Добавление'),
    );

    $this->pageTitle = Yii::t('testlinear', 'Ответы - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testlinear', 'Управление ответами'), 'url' => array('/testlinear/answerBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testlinear', 'Добавить ответ'), 'url' => array('/testlinear/answerBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testlinear', 'Ответы'); ?>
        <small><?php echo Yii::t('testlinear', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>