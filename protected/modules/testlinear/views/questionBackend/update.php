<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testlinear')->getCategory() => array(),
        Yii::t('testlinear', 'Вопросы') => array('/testlinear/questionBackend/index'),
        $model->id => array('/testlinear/questionBackend/view', 'id' => $model->id),
        Yii::t('testlinear', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('testlinear', 'Вопросы - редактирование');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testlinear', 'Управление вопросами'), 'url' => array('/testlinear/questionBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testlinear', 'Добавить вопрос'), 'url' => array('/testlinear/questionBackend/create')),
        array('label' => Yii::t('testlinear', 'Вопрос') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('testlinear', 'Редактирование вопроса'), 'url' => array(
            '/testlinear/questionBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('testlinear', 'Просмотреть вопрос'), 'url' => array(
            '/testlinear/questionBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('testlinear', 'Удалить вопрос'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/testlinear/questionBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('testlinear', 'Вы уверены, что хотите удалить вопрос?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testlinear', 'Редактирование') . ' ' . Yii::t('testlinear', 'вопроса'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>