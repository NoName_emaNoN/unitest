<?php

use yupe\components\WebModule;

class TestlinearModule extends WebModule
{
    const VERSION = '0.9';

    public $uploadPath = 'testlinear';
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    public $minSize = 0;
    public $maxSize = 5368709120;
    public $maxFiles = 1;

    public function getDependencies()
    {
        return array();
    }

    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type'    => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'TestlinearModule.testlinear',
                    'Directory "{dir}" is not accessible for write! {link}',
                    [
                        '{dir}'  => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('TestlinearModule.testlinear', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => 'testlinear',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    public function getEditableParams()
    {
        return array(
            'adminMenuOrder',
            'uploadPath',
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        );
    }

    public function getParamsLabels()
    {
        return array(
            'adminMenuOrder'    => Yii::t('TestlinearModule.testlinear', 'Menu items order'),
            'editor'            => Yii::t('TestlinearModule.testlinear', 'Visual Editor'),
            'uploadPath'        => Yii::t(
                'TestlinearModule.testlinear',
                'Uploading files catalog (relatively {path})',
                [
                    '{path}' => Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . Yii::app()->getModule(
                            "yupe"
                        )->uploadPath
                ]
            ),
            'allowedExtensions' => Yii::t('TestlinearModule.testlinear', 'Accepted extensions (separated by comma)'),
            'minSize'           => Yii::t('TestlinearModule.testlinear', 'Minimum size (in bytes)'),
            'maxSize'           => Yii::t('TestlinearModule.testlinear', 'Maximum size (in bytes)'),
        );
    }

    public function getIsInstallDefault()
    {
        return true;
    }

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getCategory()
    {
        return Yii::t('TestlinearModule.testlinear', 'Контент');
    }

    public function getName()
    {
        return Yii::t('TestlinearModule.testlinear', 'Тесты линейные');
    }

    public function getDescription()
    {
        return Yii::t('TestlinearModule.testlinear', 'Модуль для создания и управления линейными тестами');
    }

    public function getAuthor()
    {
        return Yii::t('TestlinearModule.testlinear', 'Mikhail Chemezov');
    }

    public function getAuthorEmail()
    {
        return Yii::t('TestlinearModule.testlinear', 'michlenanosoft@gmail.com');
    }

    public function getUrl()
    {
        return Yii::t('TestlinearModule.testlinear', 'http://zexed.net');
    }

    public function getIcon()
    {
        return 'fa fa-fw fa-file';
    }

    public function isMultiLang()
    {
        return false;
    }

    public function init()
    {
        parent::init();

        $this->setImport(
            array(
                'testlinear.models.*',
                'testlinear.components.*',
            )
        );
    }

    public function getNavigation()
    {
        return array(
            ['label' => 'Тесты'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TestlinearModule.testlinear', 'Список тестов'),
                'url'   => array('/testlinear/testBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TestlinearModule.testlinear', 'Добавить тест'),
                'url'   => array('/testlinear/testBackend/create')
            ),
            ['label' => 'Вопросы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TestlinearModule.testlinear', 'Список вопросов'),
                'url'   => array('/testlinear/questionBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TestlinearModule.testlinear', 'Добавить вопрос'),
                'url'   => array('/testlinear/questionBackend/create')
            ),
            ['label' => 'Ответы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TestlinearModule.testlinear', 'Список ответов'),
                'url'   => array('/testlinear/answerBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TestlinearModule.testlinear', 'Добавить ответ'),
                'url'   => array('/testlinear/answerBackend/create')
            ),
            ['label' => 'Исходы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TestlinearModule.testlinear', 'Список исходов'),
                'url'   => array('/testlinear/outcomeBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TestlinearModule.testlinear', 'Добавить исход'),
                'url'   => array('/testlinear/outcomeBackend/create')
            ),
        );
    }

    public function getAdminPageLink()
    {
        return '/testlinear/testBackend/index';
    }

    public function getAuthItems()
    {
        return array(
            array(
                'name'        => 'Testlinear.TestlinearManager',
                'description' => Yii::t('TestlinearModule.testlinear', 'Manage categories'),
                'type'        => AuthItem::TYPE_TASK,
                'items'       => array(
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testlinear.TestlinearBackend.Create',
                        'description' => Yii::t('TestlinearModule.testlinear', 'Creating testlinear')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testlinear.TestlinearBackend.Delete',
                        'description' => Yii::t('TestlinearModule.testlinear', 'Removing testlinear')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testlinear.TestlinearBackend.Index',
                        'description' => Yii::t('TestlinearModule.testlinear', 'List of categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testlinear.TestlinearBackend.Update',
                        'description' => Yii::t('TestlinearModule.testlinear', 'Editing categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testlinear.TestlinearBackend.Inline',
                        'description' => Yii::t('TestlinearModule.testlinear', 'Editing categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testlinear.TestlinearBackend.View',
                        'description' => Yii::t('TestlinearModule.testlinear', 'Viewing categories')
                    ),
                )
            )
        );
    }
}
