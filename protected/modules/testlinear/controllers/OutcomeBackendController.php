<?php

/**
 * Класс QuestionOutcomeBackendController:
 *
 * @category Yupeyupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team
 * <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class OutcomeBackendController extends yupe\components\controllers\BackController
{
    public function filters()
    {
        return [
            'postOnly + delete',
        ];
    }

    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Outcome',
                'validAttributes' => ['status']
            ],
            'view'   => [
                'class'      => '\yupe\components\actions\ViewAction',
                'modelClass' => 'Outcome',
            ],
            'update' => [
                'class'      => '\yupe\components\actions\UpdateAction',
                'modelClass' => 'Outcome',
            ],
            'index'  => [
                'class'      => '\yupe\components\actions\IndexAction',
                'modelClass' => 'Outcome',
            ],
            'create' => [
                'class'      => '\yupe\components\actions\CreateAction',
                'modelClass' => 'Outcome',
            ],
            'delete' => [
                'class'      => '\yupe\components\actions\DeleteAction',
                'modelClass' => 'Outcome',
            ],
        ];
    }
}
