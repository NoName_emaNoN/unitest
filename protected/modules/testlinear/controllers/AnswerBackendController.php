<?php

/**
 * Класс AnswerBackendController:
 *
 * @category Yupeyupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team
 * <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class AnswerBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('admin')),
            array('allow', 'actions' => array('create'), 'roles' => array('Question.QuestionBackend.Create')),
            array('allow', 'actions' => array('delete'), 'roles' => array('Question.QuestionBackend.Delete')),
            array('allow', 'actions' => array('index'), 'roles' => array('Question.QuestionBackend.Index')),
            array('allow', 'actions' => array('inline'), 'roles' => array('Question.QuestionBackend.Update')),
            array('allow', 'actions' => array('update'), 'roles' => array('Question.QuestionBackend.Update')),
            array('allow', 'actions' => array('view'), 'roles' => array('Question.QuestionBackend.View')),
            array('deny')
        );
    }

    public function actions()
    {
        return array(
            'inline' => array(
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Answer',
                'validAttributes' => array('text', 'status', 'question_id', 'outcome_id', 'outcome_weight'),
            ),
            'toggle' => [
                'class'     => 'booster.actions.TbToggleAction',
                'modelName' => 'Answer',
            ],
            'create' => [
                'class'          => '\yupe\components\actions\CreateAction',
                'modelClass'     => 'Answer',
                'successMessage' => Yii::t('question', 'Запись добавлена!'),
            ],
            'update' => [
                'class'          => '\yupe\components\actions\UpdateAction',
                'modelClass'     => 'Answer',
                'successMessage' => Yii::t('question', 'Запись обновлена!')
            ],
            'delete' => [
                'class'          => '\yupe\components\actions\DeleteAction',
                'modelClass'     => 'Answer',
                'successMessage' => Yii::t('question', 'Запись удалена!'),
            ],
            'view'   => [
                'class'        => '\yupe\components\actions\ViewAction',
                'modelClass'   => 'Answer',
                'errorMessage' => Yii::t('question', 'Запрошенная страница не найдена.'),
            ],
            'index'  => [
                'class'      => '\yupe\components\actions\IndexAction',
                'modelClass' => 'Answer',
            ],
        );
    }
}
