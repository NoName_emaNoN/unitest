<?php

class QuestionController extends \yupe\components\controllers\FrontController
{
    public function filters()
    {
        return array(
//            'accessControl',
//            'postOnly + ajaxQuestion'
        );
    }

    public function accessRules()
    {
        return array(
//            array(
//                'allow',
//                'actions' => array('question', 'check', 'start', 'reset', 'result'),
//                'users'   => array('@'),
//            ),
//            array(
//                'deny',
//                'users' => array('*'),
//            )
        );
    }

    public function actionStart($test_id)
    {
        $test = $this->loadTest($test_id);
        $new = false;

        $session = UserSession::model()->findByAttributes(
            array(
                'user_id' => Yii::app()->user->getUid(),
                'status'  => UserSession::STATUS_OPENED,
                'test_id' => $test->id,
            )
        );

        if (!$session) {
            $session = new UserSession();
            $session->test_id = $test->id;
            $session->save();
            $new = true;
        }

        $this->redirect(array('question', 'test_id' => $test->id, '#' => $new ? 'test' : null));
    }

    public function actionQuestion($test_id)
    {
        $session = $this->getSession($test_id);
        $test = $this->loadTest($test_id);

        Yii::app()->theme = $test->theme;

        if ($session->answerCount >= Question::model()->active()->test($test_id)->count()) {
            //end
            $session->close();

            if ($session->save()) {
                $this->redirect($session->url);
            } else {
                throw new CHttpException(500, 'Произошла ошибка при сохранении сессии');
            }
        }

        /* Let search a next question */

        $criteria = new CDbCriteria();

        if ($session->answerCount > 0) {
            $questions = array();

            foreach ($session->answers as $answer) {
                $questions[] = $answer->question_id;
            }

            $criteria->addNotInCondition('id', $questions);
        }

        $criteria->compare('test_id', $test_id);
        $criteria->order = 't.id ASC';

        $answer = new UserSessionAnswer();

        if (isset($_POST['UserSessionAnswer'])) {
            $answer->attributes = $_POST['UserSessionAnswer'];
            $answer->session_id = $session->id;

            if ($answer->save()) {
//                $session->refresh();

                $this->redirect(['question', 'test_id' => $test_id, '#' => 'test']);
            } else {
                throw new CHttpException(500, 'Произошла ошибка при сохранении ответа');
            }
        } else {
            /* @var $question Question */
            $question = Question::model()->find($criteria);

            if (!$question) {
                // Закончились вопросы :(
                $session->close();

                if ($session->save()) {
                    $this->redirect($session->url);
                } else {
                    throw new CHttpException(500, 'Произошла ошибка при сохранении сессии');
                }
            }

            $answer->session_id = $session->id;
            $answer->question_id = $question->id;

            $this->render(
                'question',
                array(
                    'question' => $question,
                    'session'  => $session,
                    'answer'   => $answer,
                    'test'     => $test,
                )
            );
        }
    }

//    public function actionReset()
//    {
//        $session = $this->getSession();
//
//        $session->status = UserSession::STATUS_ABORTED;
//        $session->close_date = new CDbExpression('NOW()');
//
//        if ($session->save()) {
//            $this->redirect(array('/question/question/start'));
//        } else {
//            throw new CHttpException(500, 'Произошла ошибка при сохранении сессии');
//        }
//    }
//
    public function actionResult($session)
    {
        $session = UserSession::model()->findByPk($session);

        if (!$session) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        Yii::app()->theme = $session->test->theme;

        $this->render('result', array('session' => $session));
    }

    /**
     * @param integer $test_id
     *
     * @return UserSession
     */
    public function getSession($test_id)
    {
        /* @var $session UserSession */
        $session = UserSession::model()->findByAttributes(
            array(
                'user_id' => Yii::app()->user->getUid(),
                'status'  => UserSession::STATUS_OPENED,
                'test_id' => $test_id,
            )
        );

        if (!$session) {
            $this->redirect(array('/'));
        }

        return $session;
    }

//    public function actionSubscribe()
//    {
//        $model = new SubscribeForm();
//
//        if (isset($_POST['SubscribeForm'])) {
//            $model->attributes = $_POST['SubscribeForm'];
//
//            if ($model->validate()) {
//                $queue = new Queue();
//                $queue->task = $model->email;
//                $queue->worker = 0;
//                if ($queue->save()) {
//                    $this->redirect(array('/'));
//                }
//            }
//        }
//
//        $this->render('subscribe', array('model' => $model));
//    }

    /**
     * @param integer $id
     * @return Test
     * @throws CHttpException
     */
    protected function loadTest($id)
    {
        $model = Test::model()->active()->findByPk($id);

        if (!$model) {
            throw new CHttpException(404, 'Тест не найден');
        }

        return $model;
    }

}