<?php

class m130814_202722_create_user_session_answer_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{testlinear_user_session_answer}}',
            array(
                'id'          => 'pk',
                'session_id'  => 'integer NOT NULL',
                'question_id' => 'integer NOT NULL',
                'answer_id'   => 'integer NOT NULL',
            ),
            $this->getOptions()
        );

        $this->addForeignKey('fk_{{testlinear_user_session_answer}}_session_id', '{{testlinear_user_session_answer}}', 'session_id', '{{testlinear_user_session}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{testlinear_user_session_answer}}_question_id', '{{testlinear_user_session_answer}}', 'question_id', '{{testlinear_question}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{testlinear_user_session_answer}}_answer_id', '{{testlinear_user_session_answer}}', 'answer_id', '{{testlinear_answer}}', 'id', 'CASCADE');

        $this->createIndex('ux_{{testlinear_user_session_answer}}_session_id_question_id', '{{testlinear_user_session_answer}}', 'session_id, question_id', true);
    }

    public function down()
    {
        $this->dropTableWithForeignKeys('{{testlinear_user_session_answer}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}