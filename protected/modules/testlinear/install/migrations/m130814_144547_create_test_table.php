<?php

class m130814_144547_create_test_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{testlinear_test}}',
            array(
                'id'     => 'pk',
                'name'   => 'string',
                'text'   => 'text',
                'status' => 'tinyint(1) NOT NULL DEFAULT 1',
                'image'  => 'string',
            ),
            $this->getOptions()
        );

        $this->createIndex('ix_{{testlinear_test}}_status', '{{testlinear_test}}', 'status');
    }

    public function down()
    {
        $this->dropTableWithForeignKeys('{{testlinear_test}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}