<?php

class m130814_200823_create_user_session_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{testlinear_user_session}}',
            array(
                'id'         => 'pk',
                'user_id'    => 'string NOT NULL',
                'test_id'    => 'integer NOT NULL',
                'status'     => 'tinyint(1) NOT NULL DEFAULT 0',
                'close_date' => 'DATETIME',
            ),
            $this->getOptions()
        );

        $this->createIndex('ix_{{testlinear_user_session}}_status', '{{testlinear_user_session}}', 'status');

        $this->addForeignKey('fk_{{testlinear_user_session}}_test_id', '{{testlinear_user_session}}', 'test_id', '{{testlinear_test}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{testlinear_user_session}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}