<?php

class m130814_144548_create_question_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{testlinear_question}}',
            array(
                'id'      => 'pk',
                'text'    => 'text',
                'status'  => 'tinyint(1) NOT NULL DEFAULT 1',
                'test_id' => 'integer NOT NULL',
                'image'   => 'string DEFAULT NULL',
            ),
            $this->getOptions()
        );

        $this->createTable(
            '{{testlinear_answer}}',
            [
                'id'              => 'pk',
                'question_id'     => 'integer NOT NULL',
                'text'            => 'string NOT NULL',
                'is_right_answer' => 'boolean NOT NULL DEFAULT FALSE',
            ],
            $this->getOptions()
        );

        $this->createTable(
            '{{testlinear_outcome}}',
            [
                'id'                => 'pk',
                'name'              => 'string NOT NULL',
                'text'              => 'text',
                'css_class'         => 'string NOT NULL',
                'image'             => 'string DEFAULT NULL',
                'min_right_answers' => 'integer NOT NULL DEFAULT 0',
                'max_right_answers' => 'integer NOT NULL DEFAULT 0',
                'test_id'           => 'integer NOT NULL',
            ],
            $this->getOptions()
        );

        $this->createIndex('ix_{{testlinear_question}}_status', '{{testlinear_question}}', 'status');
        $this->createIndex('ix_{{testlinear_question}}_test_id', '{{testlinear_question}}', 'test_id');

        $this->createIndex('ix_{{testlinear_answer}}_question_id', '{{testlinear_answer}}', 'question_id');

        $this->addForeignKey('fk_{{testlinear_question}}_test_id', '{{testlinear_question}}', 'test_id', '{{testlinear_test}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{testlinear_answer}}_question_id', '{{testlinear_answer}}', 'question_id', '{{testlinear_question}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{testlinear_outcome}}_test_id', '{{testlinear_outcome}}', 'test_id', '{{testlinear_test}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTableWithForeignKeys('{{testlinear_answer}}');
        $this->dropTableWithForeignKeys('{{testlinear_outcome}}');
        $this->dropTableWithForeignKeys('{{testlinear_question}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}