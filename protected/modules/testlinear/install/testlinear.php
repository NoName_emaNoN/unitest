<?php
return array(
    'module'    => array(
        'class'  => 'application.modules.testlinear.TestlinearModule',
        'layout' => '//layouts/column1',
    ),
    'import'    => array(
        'application.modules.testlinear.models.*',
    ),
    'component' => array(),
    'rules'     => array(
        '/linear/<test_id:\d+>/start'    => '/testlinear/question/start',
        '/linear/<test_id:\d+>/question' => '/testlinear/question/question',
        '/linear/<test_id:\d+>/reset'    => '/testlinear/question/reset',
        '/linear/result/<session:\d+>'   => '/testlinear/question/result',
    ),
);