<?php
/**
 * Отображение для index:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
$this->breadcrumbs = array(
    Yii::app()->getModule('testweight')->getCategory() => array(),
    Yii::t('testweight', 'Вопросы')                    => array('/testweight/questionBackend/index'),
    Yii::t('testweight', 'Управление'),
);

$this->pageTitle = Yii::t('testweight', 'Вопросы - управление');

$this->menu = array(
    array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testweight', 'Управление вопросами'), 'url' => array('/testweight/questionBackend/index')),
    array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testweight', 'Добавить вопрос'), 'url' => array('/testweight/questionBackend/create')),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testweight', 'Вопросы'); ?>
        <small><?php echo Yii::t('testweight', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('testweight', 'Поиск вопросов'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('question-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', array('model' => $model));
    ?>
</div>

<br/>

<p> <?php echo Yii::t('testweight', 'В данном разделе представлены средства управления вопросами'); ?>
</p>

<?php
$this->widget(
    'yupe\widgets\CustomGridView',
    array(
        'id'           => 'question-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            [
                'name'        => 'id',
                'htmlOptions' => ['width' => '60'],
            ],
            [
                'name'   => 'image',
                'type'   => 'raw',
                'value'  => 'CHtml::image($data->getImageUrl(75, 75), "", array("width" => 75, "height" => 75))',
                'filter' => false
            ],
            'text',
            array(
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Question::STATUS_ACTIVE     => ['class' => 'label-success'],
                    Question::STATUS_NOT_ACTIVE => ['class' => 'label-default'],
                ],
            ),
            [
                'name'   => 'test_id',
                'filter' => CHtml::listData(Test::model()->findAll(), 'id', 'name'),
                'value'  => '$data->test->name',
            ],
            [
                'header' => $model->getAttributeLabel('answers'),
                'type'   => 'raw',
                'value'  => 'CHtml::link($data->answersCount, ["/testweight/answerBackend/index", "Answer[question_id]" => $data->id])',
            ],
            array(
                'class' => 'yupe\widgets\CustomButtonColumn',
            ),
        ),
    )
); ?>
