<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testweight')->getCategory() => array(),
        Yii::t('testweight', 'Вопросы') => array('/testweight/questionBackend/index'),
        $model->id => array('/testweight/questionBackend/view', 'id' => $model->id),
        Yii::t('testweight', 'Редактирование'),
    );

    $this->pageTitle = Yii::t('testweight', 'Вопросы - редактирование');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testweight', 'Управление вопросами'), 'url' => array('/testweight/questionBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testweight', 'Добавить вопрос'), 'url' => array('/testweight/questionBackend/create')),
        array('label' => Yii::t('testweight', 'Вопрос') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('testweight', 'Редактирование вопроса'), 'url' => array(
            '/testweight/questionBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('testweight', 'Просмотреть вопрос'), 'url' => array(
            '/testweight/questionBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('testweight', 'Удалить вопрос'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/testweight/questionBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('testweight', 'Вы уверены, что хотите удалить вопрос?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testweight', 'Редактирование') . ' ' . Yii::t('testweight', 'вопроса'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>