<?php
/**
 * Отображение для _form:
 *
 * @category YupeView
 * @package  yupe
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $model Answer
 * @var $form TbActiveForm
 * @var $this AnswerBackendController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    array(
        'id'                     => 'question-answer-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => array('class' => 'well'),
    )
);
?>

    <div class="alert alert-info">
        <?php echo Yii::t('question', 'Поля, отмеченные'); ?>
        <span class="required">*</span>
        <?php echo Yii::t('question', 'обязательны.'); ?>
    </div>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->dropDownListGroup($model, 'question_id', array('widgetOptions' => array('data' => CHtml::listData(Question::model()->findAll(), 'id', 'text'), 'htmlOptions' => ['empty' => '-- Выберите вопрос --']))); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?php echo $form->textFieldGroup(
                $model,
                'text',
                array('widgetOptions' => array('htmlOptions' => array('class' => 'popover-help', 'data-original-title' => $model->getAttributeLabel('text'), 'data-content' => $model->getAttributeDescription('text'))))
            ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7 answer-outcome-block">
            <p><b>Исходы</b></p>

            <?php foreach ($model->answerOutcomes as $ao): ?>
                <div class="row answer-outcome-row">
                    <div class="col-xs-4">
                        <?= CHtml::dropDownList(get_class($model) . '[outcomes][outcome_id][]', $ao->outcome_id, CHtml::listData(Outcome::model()->findAll(), 'id', 'name'), ['class' => 'form-control']); ?>
                    </div>
                    <div class="col-xs-2">
                        <?= CHtml::textField(get_class($model) . '[outcomes][weight][]', $ao->weight, ['class' => 'form-control', 'placeholder' => AnswerOutcome::model()->getAttributeLabel('weight')]); ?>
                    </div>
                    <div class="col-xs-1">
                        <a class="btn btn-default js-add-answer-row" href="#"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php if ($model->isNewRecord || $model->answerOutcomesCount == 0): ?>
                <div class="row answer-outcome-row">
                    <div class="col-xs-4">
                        <?= CHtml::dropDownList(get_class($model) . '[outcomes][outcome_id][]', '', CHtml::listData(Outcome::model()->findAll(), 'id', 'name'), ['class' => 'form-control']); ?>
                    </div>
                    <div class="col-xs-2">
                        <?= CHtml::textField(get_class($model) . '[outcomes][weight][]', '', ['class' => 'form-control', 'placeholder' => AnswerOutcome::model()->getAttributeLabel('weight')]); ?>
                    </div>
                    <div class="col-xs-1">
                        <a class="btn btn-default js-add-answer-row" href="#"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <br/>


    <script>
        $(document).on('click', '.js-add-answer-row', function () {
            $('.answer-outcome-row:first').clone().appendTo('.answer-outcome-block');

            return false;
        });
    </script>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => Yii::t('question', 'Сохранить ответ и продолжить'),
    )
); ?>

<?php
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'buttonType'  => 'submit',
        'htmlOptions' => array('name' => 'submit-type', 'value' => 'index'),
        'label'       => Yii::t('question', 'Сохранить ответ и закрыть'),
    )
); ?>

<?php $this->endWidget(); ?>