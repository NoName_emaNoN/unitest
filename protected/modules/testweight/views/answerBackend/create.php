<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testweight')->getCategory() => array(),
        Yii::t('testweight', 'Ответы') => array('/testweight/answerBackend/index'),
        Yii::t('testweight', 'Добавление'),
    );

    $this->pageTitle = Yii::t('testweight', 'Ответы - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testweight', 'Управление ответами'), 'url' => array('/testweight/answerBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testweight', 'Добавить ответ'), 'url' => array('/testweight/answerBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testweight', 'Ответы'); ?>
        <small><?php echo Yii::t('testweight', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>