<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testweight')->getCategory() => array(),
        Yii::t('testweight', 'Ответы') => array('/testweight/answerBackend/index'),
        $model->id,
    );

    $this->pageTitle = Yii::t('testweight', 'Ответы - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testweight', 'Управление ответами'), 'url' => array('/testweight/answerBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testweight', 'Добавить ответ'), 'url' => array('/testweight/answerBackend/create')),
        array('label' => Yii::t('testweight', 'Ответ') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('testweight', 'Редактирование ответа'), 'url' => array(
            '/testweight/answerBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('testweight', 'Просмотреть ответ'), 'url' => array(
            '/testweight/answerBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('testweight', 'Удалить ответ'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/testweight/answerBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('testweight', 'Вы уверены, что хотите удалить ответ?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testweight', 'Просмотр') . ' ' . Yii::t('testweight', 'ответа'); ?>        <br/>
        <small>&laquo;<?php echo $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'question_id',
        'text',
        'outcome_id',
        'outcome_weight',
),
)); ?>
