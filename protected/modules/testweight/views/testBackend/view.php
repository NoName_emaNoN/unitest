<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testweight')->getCategory() => array(),
        Yii::t('testweight', 'Тесты') => array('/testweight/testBackend/index'),
        $model->name,
    );

    $this->pageTitle = Yii::t('testweight', 'Тесты - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testweight', 'Управление тестами'), 'url' => array('/testweight/testBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testweight', 'Добавить тест'), 'url' => array('/testweight/testBackend/create')),
        array('label' => Yii::t('testweight', 'Тест') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('testweight', 'Редактирование теста'), 'url' => array(
            '/testweight/testBackend/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('testweight', 'Просмотреть тест'), 'url' => array(
            '/testweight/testBackend/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('testweight', 'Удалить тест'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/testweight/testBackend/delete', 'id' => $model->id),
            'confirm' => Yii::t('testweight', 'Вы уверены, что хотите удалить тест?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testweight', 'Просмотр') . ' ' . Yii::t('testweight', 'теста'); ?>        <br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'name',
        'text',
        'status',
        'image',
),
)); ?>
