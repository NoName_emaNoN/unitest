<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testweight')->getCategory() => array(),
        Yii::t('testweight', 'Тесты') => array('/testweight/testBackend/index'),
        Yii::t('testweight', 'Добавление'),
    );

    $this->pageTitle = Yii::t('testweight', 'Тесты - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testweight', 'Управление тестами'), 'url' => array('/testweight/testBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testweight', 'Добавить тест'), 'url' => array('/testweight/testBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testweight', 'Тесты'); ?>
        <small><?php echo Yii::t('testweight', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>