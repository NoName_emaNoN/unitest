<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('testweight')->getCategory() => array(),
        Yii::t('testweight', 'Исходы') => array('/testweight/outcomeBackend/index'),
        Yii::t('testweight', 'Добавление'),
    );

    $this->pageTitle = Yii::t('testweight', 'Исходы - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('testweight', 'Управление исходами'), 'url' => array('/testweight/outcomeBackend/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('testweight', 'Добавить исход'), 'url' => array('/testweight/outcomeBackend/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('testweight', 'Исходы'); ?>
        <small><?php echo Yii::t('testweight', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>