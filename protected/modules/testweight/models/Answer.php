<?php

/**
 * This is the model class for table "{{testweight_answer}}".
 *
 * The followings are the available columns in table '{{testweight_answer}}':
 * @property integer $id
 * @property integer $question_id
 * @property string $text
 *
 * The followings are the available model relations:
 * @property Question $question
 * @property AnswerOutcome[] $answerOutcomes
 * @property integer $answerOutcomesCount
 */
class Answer extends yupe\models\YModel
{
    public $outcomes;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{testweight_answer}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('question_id, text', 'required'),
            array('question_id', 'numerical', 'integerOnly' => true),
            array('text', 'length', 'max' => 255),
            ['outcomes', 'safe'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, question_id, text', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'answerOutcomes'      => [self::HAS_MANY, 'AnswerOutcome', 'answer_id'],
            'answerOutcomesCount' => [self::STAT, 'AnswerOutcome', 'answer_id'],
            'question'            => array(self::BELONGS_TO, 'Question', 'question_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'          => 'ID',
            'question_id' => 'Вопрос',
            'text'        => 'Текст',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.question_id', $this->question_id);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Answer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function afterFind()
    {
        parent::afterFind();

        foreach ($this->answerOutcomes as $ao) {
            $this->outcomes['weight'][] = $ao->weight;
            $this->outcomes['outcome_id'][] = $ao->outcome_id;
        }
    }

    protected function beforeSave()
    {
        return parent::beforeSave();
    }

    public function afterSave()
    {
        if (isset($this->outcomes['outcome_id'])) {
            foreach ($this->answerOutcomes as $ao) {
                $ao->delete();
            }

            foreach (array_keys($this->outcomes['outcome_id']) as $key) {
                $ao = new AnswerOutcome();

                $ao->outcome_id = $this->outcomes['outcome_id'][$key];
                $ao->weight = $this->outcomes['weight'][$key];
                $ao->answer_id = $this->id;

                $ao->save();
            }
        }

        parent::afterSave();
    }
}
