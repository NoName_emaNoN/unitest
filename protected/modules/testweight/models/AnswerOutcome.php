<?php

/**
 * This is the model class for table "{{testweight_answer_outcome}}".
 *
 * The followings are the available columns in table '{{testweight_answer_outcome}}':
 * @property integer $answer_id
 * @property integer $outcome_id
 * @property integer $weight
 */
class AnswerOutcome extends yupe\models\YModel
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{testweight_answer_outcome}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('answer_id, outcome_id', 'required'),
            array('answer_id, outcome_id, weight', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('answer_id, outcome_id, weight', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'answer_id'  => 'Ответ',
            'outcome_id' => 'Исход',
            'weight'     => 'Вес исхода',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('answer_id', $this->answer_id);
        $criteria->compare('outcome_id', $this->outcome_id);
        $criteria->compare('weight', $this->weight);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AnswerOutcome the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
