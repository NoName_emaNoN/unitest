<?php

/**
 * This is the model class for table "{{testweight_test}}".
 *
 * The followings are the available columns in table '{{testweight_test}}':
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $status
 * @property string $image
 * @property string $theme
 *
 * The followings are the available model relations:
 * @property Question[] $questions
 * @property UserSession[] $sessions
 * @property integer $questionsCount
 *
 * @method Test active()
 */
class Test extends yupe\models\YModel
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{testweight_test}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            ['name, status, theme', 'required'],
            array('status', 'numerical', 'integerOnly' => true),
            array('name, theme', 'length', 'max' => 255),
            array('text', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, text, status, image, theme', 'safe', 'on' => 'search'),
        );
    }

    public function behaviors()
    {
        $module = Yii::app()->getModule('testweight');

        return [
            'imageUpload' => [
                'class'          => 'yupe\components\behaviors\ImageUploadBehavior',
                'scenarios'      => ['insert', 'update'],
                'attributeName'  => 'image',
                'minSize'        => $module->minSize,
                'maxSize'        => $module->maxSize,
                'types'          => $module->allowedExtensions,
                'uploadPath'     => $module->uploadPath,
                'resizeOnUpload' => false,
            ],
        ];
    }

    public function scopes()
    {
        return [
            'active' => [
                'condition' => $this->tableAlias . '.status = :status',
                'params'    => [':status' => self::STATUS_ACTIVE],
            ],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'questions'      => array(self::HAS_MANY, 'Question', 'test_id'),
            'questionsCount' => array(self::STAT, 'Question', 'test_id'),
            'sessions'       => array(self::HAS_MANY, 'UserSession', 'test_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'        => 'ID',
            'name'      => 'Название',
            'text'      => 'Текст',
            'status'    => 'Статус',
            'image'     => 'Изображение',
            'questions' => 'Вопросы',
            'theme'     => 'Тема',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare($this->tableAlias . '.id', $this->id);
        $criteria->compare($this->tableAlias . '.name', $this->name, true);
        $criteria->compare($this->tableAlias . '.text', $this->text, true);
        $criteria->compare($this->tableAlias . '.status', $this->status);
        $criteria->compare($this->tableAlias . '.image', $this->image, true);
        $criteria->compare($this->tableAlias . '.theme', $this->theme, true);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Test the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getStatusList()
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Скрыт',
            self::STATUS_ACTIVE     => 'Активен',
        ];
    }

    public function getStatus()
    {
        return $this->getStatusList()[$this->status];
    }

    public function getUrl($absolute = false)
    {
        return call_user_func([Yii::app(), $absolute ? 'createAbsoluteUrl' : 'createUrl'], '/testweight/question/start', ['test_id' => $this->id, '#' => 'test']);
    }
}
