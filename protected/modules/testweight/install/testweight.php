<?php
return array(
    'module'    => array(
        'class'  => 'application.modules.testweight.TestweightModule',
        'layout' => '//layouts/column1',
    ),
    'import'    => array(
        'application.modules.testweight.models.*',
    ),
    'component' => array(),
    'rules'     => array(
        '/weight/<test_id:\d+>/start'    => '/testweight/question/start',
        '/weight/<test_id:\d+>/question' => '/testweight/question/question',
        '/weight/<test_id:\d+>/reset'    => '/testweight/question/reset',
        '/weight/result/<session:\d+>'   => '/testweight/question/result',
    ),
);