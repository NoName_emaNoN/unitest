<?php

class m150303_092647_create_answer_outcome_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{testweight_answer_outcome}}',
            [
                'answer_id'  => 'integer NOT NULL',
                'outcome_id' => 'integer NOT NULL',
                'weight'     => 'tinyint(3) NOT NULL DEFAULT 1',
            ],
            $this->getOptions()
        );

        $this->addPrimaryKey('pk', '{{testweight_answer_outcome}}', 'answer_id,outcome_id');

        $this->addForeignKey('fk_{{testweight_answer_outcome}}_outcome_id', '{{testweight_answer_outcome}}', 'outcome_id', '{{testweight_outcome}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{testweight_answer_outcome}}_answer_id', '{{testweight_answer_outcome}}', 'answer_id', '{{testweight_answer}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTableWithForeignKeys('{{testweight_answer_outcome}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}