<?php

class m150310_102705_add_theme_column extends CDbMigration
{
    public function up()
    {
        $this->addColumn('{{testweight_test}}', 'theme', 'string NOT NULL DEFAULT "default"');
    }

    public function down()
    {
        $this->dropColumn('{{testweight_test}}', 'theme');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}