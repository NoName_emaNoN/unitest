<?php

class m130814_202722_create_user_session_answer_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{testweight_user_session_answer}}',
            array(
                'id'          => 'pk',
                'session_id'  => 'integer NOT NULL',
                'question_id' => 'integer NOT NULL',
                'answer_id'   => 'integer NOT NULL',
            ),
            $this->getOptions()
        );

        $this->addForeignKey('fk_{{testweight_user_session_answer}}_session_id', '{{testweight_user_session_answer}}', 'session_id', '{{testweight_user_session}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{testweight_user_session_answer}}_question_id', '{{testweight_user_session_answer}}', 'question_id', '{{testweight_question}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{testweight_user_session_answer}}_answer_id', '{{testweight_user_session_answer}}', 'answer_id', '{{testweight_answer}}', 'id', 'CASCADE');

        $this->createIndex('ux_{{testweight_user_session_answer}}_session_id_question_id', '{{testweight_user_session_answer}}', 'session_id, question_id', true);
    }

    public function down()
    {
        $this->dropTableWithForeignKeys('{{testweight_user_session_answer}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}