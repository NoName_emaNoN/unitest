<?php

class m130814_144548_create_question_table extends \yupe\components\DbMigration
{
    public function up()
    {
        $this->createTable(
            '{{testweight_question}}',
            array(
                'id'      => 'pk',
                'text'    => 'text',
                'status'  => 'tinyint(1) NOT NULL DEFAULT 1',
                'test_id' => 'integer NOT NULL',
                'image'   => 'string DEFAULT NULL',
            ),
            $this->getOptions()
        );

        $this->createTable(
            '{{testweight_answer}}',
            [
                'id'             => 'pk',
                'question_id'    => 'integer NOT NULL',
                'text'           => 'string NOT NULL',
            ],
            $this->getOptions()
        );

        $this->createTable(
            '{{testweight_outcome}}',
            [
                'id'        => 'pk',
                'name'      => 'string NOT NULL',
                'text'      => 'text',
                'css_class' => 'string NOT NULL',
                'image'     => 'string DEFAULT NULL',
            ],
            $this->getOptions()
        );

        $this->createIndex('ix_{{testweight_question}}_status', '{{testweight_question}}', 'status');
        $this->createIndex('ix_{{testweight_question}}_test_id', '{{testweight_question}}', 'test_id');

        $this->createIndex('ix_{{testweight_answer}}_question_id', '{{testweight_answer}}', 'question_id');

        $this->addForeignKey('fk_{{testweight_question}}_test_id', '{{testweight_question}}', 'test_id', '{{testweight_test}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_{{testweight_answer}}_question_id', '{{testweight_answer}}', 'question_id', '{{testweight_question}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTableWithForeignKeys('{{testweight_answer}}');
        $this->dropTableWithForeignKeys('{{testweight_outcome}}');
        $this->dropTableWithForeignKeys('{{testweight_question}}');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}