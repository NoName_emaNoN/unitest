<?php

use yupe\components\WebModule;

class TestweightModule extends WebModule
{
    const VERSION = '0.9';

    public $uploadPath = 'testweight';
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    public $minSize = 0;
    public $maxSize = 5368709120;
    public $maxFiles = 1;

    public function getDependencies()
    {
        return array();
    }

    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    public function checkSelf()
    {
        $messages = [];

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = [
                'type'    => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                    'TestweightModule.testweight',
                    'Directory "{dir}" is not accessible for write! {link}',
                    [
                        '{dir}'  => $uploadPath,
                        '{link}' => CHtml::link(
                            Yii::t('TestweightModule.testweight', 'Change settings'),
                            [
                                '/yupe/backend/modulesettings/',
                                'module' => 'testweight',
                            ]
                        ),
                    ]
                ),
            ];
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    public function getEditableParams()
    {
        return array(
            'adminMenuOrder',
            'uploadPath',
            'editor' => Yii::app()->getModule('yupe')->getEditors(),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
        );
    }

    public function getParamsLabels()
    {
        return array(
            'adminMenuOrder'    => Yii::t('TestweightModule.testweight', 'Menu items order'),
            'editor'            => Yii::t('TestweightModule.testweight', 'Visual Editor'),
            'uploadPath'        => Yii::t(
                'TestweightModule.testweight',
                'Uploading files catalog (relatively {path})',
                [
                    '{path}' => Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . Yii::app()->getModule(
                            "yupe"
                        )->uploadPath
                ]
            ),
            'allowedExtensions' => Yii::t('TestweightModule.testweight', 'Accepted extensions (separated by comma)'),
            'minSize'           => Yii::t('TestweightModule.testweight', 'Minimum size (in bytes)'),
            'maxSize'           => Yii::t('TestweightModule.testweight', 'Maximum size (in bytes)'),
        );
    }

    public function getIsInstallDefault()
    {
        return true;
    }

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getCategory()
    {
        return Yii::t('TestweightModule.testweight', 'Контент');
    }

    public function getName()
    {
        return Yii::t('TestweightModule.testweight', 'Тесты с весами');
    }

    public function getDescription()
    {
        return Yii::t('TestweightModule.testweight', 'Модуль для создания и управления тестами с весами');
    }

    public function getAuthor()
    {
        return Yii::t('TestweightModule.testweight', 'Mikhail Chemezov');
    }

    public function getAuthorEmail()
    {
        return Yii::t('TestweightModule.testweight', 'michlenanosoft@gmail.com');
    }

    public function getUrl()
    {
        return Yii::t('TestweightModule.testweight', 'http://zexed.net');
    }

    public function getIcon()
    {
        return 'fa fa-fw fa-file';
    }

    public function isMultiLang()
    {
        return false;
    }

    public function init()
    {
        parent::init();

        $this->setImport(
            array(
                'testweight.models.*',
                'testweight.components.*',
            )
        );
    }

    public function getNavigation()
    {
        return array(
            ['label' => 'Тесты'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TestweightModule.testweight', 'Список тестов'),
                'url'   => array('/testweight/testBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TestweightModule.testweight', 'Добавить тест'),
                'url'   => array('/testweight/testBackend/create')
            ),
            ['label' => 'Вопросы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TestweightModule.testweight', 'Список вопросов'),
                'url'   => array('/testweight/questionBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TestweightModule.testweight', 'Добавить вопрос'),
                'url'   => array('/testweight/questionBackend/create')
            ),
            ['label' => 'Ответы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TestweightModule.testweight', 'Список ответов'),
                'url'   => array('/testweight/answerBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TestweightModule.testweight', 'Добавить ответ'),
                'url'   => array('/testweight/answerBackend/create')
            ),
            ['label' => 'Исходы'],
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('TestweightModule.testweight', 'Список исходов'),
                'url'   => array('/testweight/outcomeBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('TestweightModule.testweight', 'Добавить исход'),
                'url'   => array('/testweight/outcomeBackend/create')
            ),
        );
    }

    public function getAdminPageLink()
    {
        return '/testweight/testBackend/index';
    }

    public function getAuthItems()
    {
        return array(
            array(
                'name'        => 'Testweight.TestweightManager',
                'description' => Yii::t('TestweightModule.testweight', 'Manage categories'),
                'type'        => AuthItem::TYPE_TASK,
                'items'       => array(
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testweight.TestweightBackend.Create',
                        'description' => Yii::t('TestweightModule.testweight', 'Creating testweight')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testweight.TestweightBackend.Delete',
                        'description' => Yii::t('TestweightModule.testweight', 'Removing testweight')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testweight.TestweightBackend.Index',
                        'description' => Yii::t('TestweightModule.testweight', 'List of categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testweight.TestweightBackend.Update',
                        'description' => Yii::t('TestweightModule.testweight', 'Editing categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testweight.TestweightBackend.Inline',
                        'description' => Yii::t('TestweightModule.testweight', 'Editing categories')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Testweight.TestweightBackend.View',
                        'description' => Yii::t('TestweightModule.testweight', 'Viewing categories')
                    ),
                )
            )
        );
    }
}
