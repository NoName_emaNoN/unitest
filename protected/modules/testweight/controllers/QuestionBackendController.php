<?php

/**
 * Класс QuestionBackendController:
 *
 * @category Yupeyupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team
 * <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class QuestionBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('admin')),
            array('allow', 'actions' => array('create'), 'roles' => array('Question.QuestionBackend.Create')),
            array('allow', 'actions' => array('delete'), 'roles' => array('Question.QuestionBackend.Delete')),
            array('allow', 'actions' => array('index'), 'roles' => array('Question.QuestionBackend.Index')),
            array('allow', 'actions' => array('inlineEdit'), 'roles' => array('Question.QuestionBackend.Update')),
            array('allow', 'actions' => array('update'), 'roles' => array('Question.QuestionBackend.Update')),
            array('allow', 'actions' => array('view'), 'roles' => array('Question.QuestionBackend.View')),
            array('deny')
        );
    }

    public function actions()
    {
        return array(
            'inline' => array(
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Question',
                'validAttributes' => array('text', 'status'),
            )
        );
    }

    /**
     * Отображает вопрос по указанному идентификатору
     *
     * @param integer $id Идинтификатор вопрос для отображения
     *
     * @return void
     */
    public function actionView($id)
    {
        $this->render('view', array('model' => $this->loadModel($id)));
    }

    /**
     * Создает новую модель вопроса.
     * Если создание прошло успешно - перенаправляет на просмотр.
     *
     * @return void
     */
    public function actionCreate()
    {
        $model = new Question;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Question'])) {
            $model->attributes = $_POST['Question'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('question', 'Запись добавлена!')
                );

                if (!isset($_POST['submit-type'])) {
                    $this->redirect(array('update', 'id' => $model->id));
                } else {
                    $this->redirect(array($_POST['submit-type']));
                }
            }
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Редактирование вопроса.
     *
     * @param integer $id Идинтификатор вопрос для редактирования
     *
     * @return void
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Question'])) {
            $model->attributes = $_POST['Question'];

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('question', 'Запись обновлена!')
                );

                if (!isset($_POST['submit-type'])) {
                    $this->redirect(array('update', 'id' => $model->id));
                } else {
                    $this->redirect(array($_POST['submit-type']));
                }
            }
        }
        $this->render('update', array('model' => $model));
    }

    /**
     * Удаляет модель вопроса из базы.
     * Если удаление прошло успешно - возвращется в index
     *
     * @param integer $id идентификатор вопроса, который нужно удалить
     *
     * @return void
     */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
// поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('question', 'Запись удалена!')
            );

// если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
        } else {
            throw new CHttpException(400, Yii::t('question', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
        }
    }

    /**
     * Управление вопросами.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Question('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Question'])) {
            $model->attributes = $_GET['Question'];
        }
        $this->render('index', array('model' => $model));
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return void
     */
    public function loadModel($id)
    {
        $model = Question::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('question', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param CModel модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(Question $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'question-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
