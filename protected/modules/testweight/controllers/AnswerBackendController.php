<?php

/**
 * Класс AnswerBackendController:
 *
 * @category Yupeyupe\components\controllers\BackController
 * @package  yupe
 * @author   Yupe Team
 * <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
class AnswerBackendController extends yupe\components\controllers\BackController
{
    public function accessRules()
    {
        return array(
            array('allow', 'roles' => array('admin')),
            array('allow', 'actions' => array('create'), 'roles' => array('Question.QuestionBackend.Create')),
            array('allow', 'actions' => array('delete'), 'roles' => array('Question.QuestionBackend.Delete')),
            array('allow', 'actions' => array('index'), 'roles' => array('Question.QuestionBackend.Index')),
            array('allow', 'actions' => array('inline'), 'roles' => array('Question.QuestionBackend.Update')),
            array('allow', 'actions' => array('update'), 'roles' => array('Question.QuestionBackend.Update')),
            array('allow', 'actions' => array('view'), 'roles' => array('Question.QuestionBackend.View')),
            array('deny')
        );
    }

    public function actions()
    {
        return array(
            'inline' => array(
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Answer',
                'validAttributes' => array('text', 'status', 'question_id', 'outcome_id', 'outcome_weight'),
            ),
            'create' => [
                'class'          => '\yupe\components\actions\CreateAction',
                'modelClass'     => 'Answer',
                'successMessage' => Yii::t('question', 'Запись добавлена!'),
            ],
            'update' => [
                'class'          => '\yupe\components\actions\UpdateAction',
                'modelClass'     => 'Answer',
                'successMessage' => Yii::t('question', 'Запись обновлена!')
            ],
            'delete' => [
                'class'          => '\yupe\components\actions\DeleteAction',
                'modelClass'     => 'Answer',
                'successMessage' => Yii::t('question', 'Запись удалена!'),
            ],
            'view'   => [
                'class'        => '\yupe\components\actions\ViewAction',
                'modelClass'   => 'Answer',
                'errorMessage' => Yii::t('question', 'Запрошенная страница не найдена.'),
            ],
        );
    }

    /**
     * Управление ответами.
     *
     * @return void
     */
    public function actionIndex()
    {
        $model = new Answer('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Answer'])) {
            $model->attributes = $_GET['Answer'];
        }
        $this->render('index', array('model' => $model));
    }

    /**
     * Возвращает модель по указанному идентификатору
     * Если модель не будет найдена - возникнет HTTP-исключение.
     *
     * @param integer идентификатор нужной модели
     *
     * @return void
     */
    public function loadModel($id)
    {
        $model = Answer::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, Yii::t('question', 'Запрошенная страница не найдена.'));
        }

        return $model;
    }

    /**
     * Производит AJAX-валидацию
     *
     * @param CModel модель, которую необходимо валидировать
     *
     * @return void
     */
    protected function performAjaxValidation(Answer $model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'question-answer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
