$(document).ready(function(){
  $('.gallery-item').click(function(e){
      e.preventDefault();
      if( $(this).children('i').hasClass('test-green') ){
        $(this).children('i').toggleClass('test-green-bg');
      } else if ( $(this).children('i').hasClass('test-red') ){
        $(this).children('i').toggleClass('test-red-bg');
      }
  });


  // PROGRESS BAR 
  
  $("#progressbar").progressbar({
      value: 20
  });

  var width_a = $("#progressbar").width();
  var width_b = $(".ui-progressbar-value").width();
  if( width_a === width_b ) {
    $(".right-progressbar-borderpink").css({
      'background':'url("img/progressbar-bg-pink-border-right-100per.jpg")',
      'right': '-1px'
    });
  }
  
});