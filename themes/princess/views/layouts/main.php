<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?php echo Yii::app()->charset; ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();

    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/style.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/jquery-ui.min.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/jquery-ui.structure.min.css');

    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery-1.11.2.min.js', CClientScript::POS_HEAD);
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/jquery-ui.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/scripts.js', CClientScript::POS_END);

    Yii::app()->getClientScript()->registerScript(
        'disney-toolbar-init',
        '
    var GOC = {
        opts: {
            disneyKey: true,
            search: true,
            css: {
                zIndex: 10000
            }
        },
        queue: []
    };

    (function (d) {

        var s = d.createElement("script");

        s.async = true;

        s.src = "http://new.disney.ru/goc/us/new.disney.ru/responsive.js";

        d.getElementsByTagName("head")[0].appendChild(s);

    })(document);
    ',
        CClientScript::POS_END
    );
    ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="pink-castle">
<!-- Content starts -->
<?= $content; ?>
<!-- flashMessages -->
<!--    --><?php //$this->widget('yupe\widgets\YFlashMessages'); ?>

<?php if (Yii::app()->hasModule('contentblock')): ?>
    <?php $this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        array("code" => "STAT", "silent" => true)
    ); ?>
<?php endif; ?>


</body>
</html>