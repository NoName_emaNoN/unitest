<?php
/**
 * @var $this QuestionController
 * @var $session UserSession
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
$outcome = $session->getOutcome();

Yii::app()->clientScript->registerMetaTag($outcome->name, null, null, ['property' => 'og:title']);
Yii::app()->clientScript->registerMetaTag($session->getUrl(true), null, null, ['property' => 'og:url']);
Yii::app()->clientScript->registerMetaTag($outcome->getImageUrl(), null, null, ['property' => 'og:image']);
Yii::app()->clientScript->registerMetaTag($outcome->text, null, null, ['property' => 'og:description']);
Yii::app()->clientScript->registerMetaTag('ru_RU', null, null, ['property' => 'og:locale']);
?>

<div class="container">
    <h1 class="main-question main-question-more-top-position"><?= $session->test->name ?></h1>
</div>

<body class="<?= $outcome->css_class ?>-page">
<!-- Content starts -->
<div class="container">
    <img class="ribbon-for-responsive" src="<?= Yii::app()->theme->getAssetsUrl() ?>/img/disney-ribbon.png" alt="">
    <img class="girl-for-responsive <?= $outcome->css_class ?>-girl" src="<?= Yii::app()->theme->getAssetsUrl() ?>/img/<?= $outcome->css_class ?>.png" alt="">

    <div class="identify-box identify-box-belle">
        <div class="identify-box-test">
            <h2 class="identify-title <?= $outcome->css_class ?>-identify-title"><?= $outcome->name ?></h2>

            <p><?= $outcome->text ?></p>
        </div>
        <div class="identify-box-social identify-box-left-position">
            <a href="#" class="facebook"><i class="facebook-icon-big"></i>Share on Facebook</a>
            <a href="#" class="pinterest"><i class="pinterest-icon-big"></i>Share on Pinterest</a>
            <a href="#" class="twitter"><i class="twitter-icon-big"></i>Share on Twitter</a>
        </div>
    </div>
</div>
<!-- Content ends -->