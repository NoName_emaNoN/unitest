<?php
/**
 * @var $this QuestionController
 * @var $question Question
 * @var $session UserSession
 * @var $answer UserSessionAnswer
 * @var $form CActiveForm
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

?>

<!-- Body -->

<div class="test-page">
    <div class="top-logo center text-center">
        <?= CHtml::image($mainAssets . '/images/mail-logo.png', 'Oнлайн-игра от Disney', array('class' => 'img-responsive')); ?>
    </div>
    <div class="cols">
        <div class="col pull-left col-right">
            <div class="board">
                <div class="board-body">
                    <?php $current = $session->answerCount + 1; ?>
                    <?php $total = Question::model()->count(); ?>
                    <ul class="progress">
                        <?php for ($i = 1; $i <= $total; $i++): ?>
                            <li class="<?= $current == $i ? 'selected' : '' ?> <?= (isset($session->answers[$current - 1]) && $session->answers[$current - 1]->isRightAnswer()) ? 'success' : '' ?>"><span><?= $i ?></span></li>
                        <?php endfor; ?>
                    </ul>
                    <div class="question">
                        <div class="t">(Выбери правильный ответ)</div>
                        <div class="js-question-area">
                            <?= CHtml::encode($question->text) ?>
                        </div>
                    </div>
                    <div class="form-body js-form-body">
                        <?php
                        $form = $this->beginWidget(
                            'CActiveForm',
                            array(
                                'id'                     => 'answer-form',
                                'enableClientValidation' => false,
                            )
                        ); ?>

                        <?= $form->hiddenField($answer, 'question_id'); ?>

                        <?php foreach ($question->getAnswers() as $key => $value): ?>
                            <div class="row">
                                <div class="input">
                                    <?= $form->radioButton($answer, 'answer', array('disabled' => true, 'value' => $key)); ?>
                                    <?= $form->labelEx($answer, 'answer', array('class' => $answer->answer == $question->answer ? 'success' : 'error')); ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php $this->endWidget(); ?>
                    </div>
                    <div class="answer-result-body hidden">
                        <div class="row">
                            <div class="input">
                                <div class="label most-success">Правильно</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input">
                                <div class="label most-error">Неправильно</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col pull-right col-left text-center">
            <div class="paff">
                <div class="marker icons-sprite icons-question"></div>
            </div>
            <span class="btn btn-2 js-submit"><span>Ответить</span></span>
            <span data-src="0" class="btn btn-3 js-next hidden"><span>Следующий вопрос</span></span>
        </div>

    </div>
</div>

<!-- End body -->