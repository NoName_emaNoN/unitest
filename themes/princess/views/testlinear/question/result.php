<?php
/**
 * @var $this QuestionController
 * @var $session UserSession
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

$total = $session->answerCount;
$rightAnswers = $session->getRightAnswersCount();
$outcome = $session->getOutcome();

Yii::app()->clientScript->registerMetaTag(Yii::app()->getModule('yupe')->siteName, null, null, ['property' => 'og:title']);
Yii::app()->clientScript->registerMetaTag($session->getUrl(true), null, null, ['property' => 'og:url']);
Yii::app()->clientScript->registerMetaTag($outcome->getImageUrl(), null, null, ['property' => 'og:image']);
Yii::app()->clientScript->registerMetaTag($outcome->text, null, null, ['property' => 'og:description']);
Yii::app()->clientScript->registerMetaTag('ru_RU', null, null, ['property' => 'og:locale']);

?>
<div class="container">
    <h1 class="main-question main-question-more-top-position"><?= $session->test->name ?></h1>
</div>

<div class="container">
    <div class="content-block">
        <h2>Вы ответили правильно на <?= Yii::t('app', '{n} вопрос|{n} вопроса|{n} вопросов', [$rightAnswers]) ?> из <?= $session->answerCount ?>!</h2>

        <p><?= $outcome->text ?></p>

        <?= CHtml::image($outcome->getImageUrl(), '', ['class' => 'princess-photo']); ?>
        <?= CHtml::link('Пройти тест еще раз', ['start', 'test_id' => $session->test_id], ['class' => 'test-again']); ?>

        <div class="social-box">
            <?= CHtml::link('<i class="facebook-icon"></i>Share on Facebook', 'https://www.facebook.com/sharer/sharer.php?u=' . $session->getUrl(true), ['class' => 'facebook', 'target' => '_blank']); ?>
            <?= CHtml::link(
                '<i class="pinterest-icon"></i>Share on Pinterest',
                'https://pinterest.com/pin/create/button/?url=' . $session->getUrl(true) . '&media=' . $outcome->getImageUrl() . '&description=' . Yii::app()->getModule('yupe')->siteName,
                ['class' => 'pinterest', 'target' => '_blank']
            ); ?>
            <?= CHtml::link(
                '<i class="twitter-icon"></i>Share on Twitter',
                'https://twitter.com/intent/tweet?url=' . $session->getUrl(true) . '&text=' . Yii::app()->getModule('yupe')->siteName,
                ['class' => 'twitter', 'target' => '_blank']
            ); ?>
        </div>
    </div>
</div>
