<?php
/**
 * @var $this QuestionController
 * @var $question Question
 * @var $session UserSession
 * @var $answer UserSessionAnswer
 * @var $form CActiveForm
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

$current = $session->answerCount + 1;
$total = Question::model()->count();
$progress = round($current / $total * 100);
?>


<div class="container">
    <h1 class="main-question main-question-more-top-position"><?= $test->name ?></h1>
</div>

<div class="container">
    <div class="content-block content-block-for-gallery">
        <?php if ($question->image): ?>
            <?= CHtml::image($question->getImageUrl(), '', ['class' => 'princess-photo princess-photo-upper']); ?>
        <?php endif; ?>
        <div class="content-gallery">
            <h2 class="favorite-activity-title"><i class="pink-number-btn"><?= $current ?></i><?= $question->text ?></h2>

            <?php
            $form = $this->beginWidget(
                'CActiveForm',
                array(
                    'id'                     => 'test-form',
                    'enableClientValidation' => true,
                )
            ); ?>

            <?= $form->hiddenField($answer, 'question_id'); ?>
            <?= $form->hiddenField($answer, 'answer_id'); ?>
            <!--            --><? //= $form->radioButtonList($answer, 'answer', $question->getAnswers(), array('template' => '<div class="row"><div class="input">{input}{label}</div></div>', 'disabled' => !$answer->isNewRecord)) ?>

            <?php $this->endWidget(); ?>

            <div class="gallery-box">
                <?php foreach ($question->answers as $value): ?>
                    <a href="#" class="gallery-item" data-answer-id="<?= $value->id ?>"><i class="test-active-block"></i> <?= $value->text ?></a>
                <?php endforeach; ?>
            </div>
            <div class="progress-bar-box">
                <div id="progressbar">
                    <i class="borders-progressbar left-progressbar-bordergrey"></i>
                </div>
                <div class="progress-numbers"><?= $current ?> of <?= $total ?></div>
            </div>
        </div>
    </div>
</div>

<script>
    <?php Yii::app()->clientScript->registerScript('progressbar', '$(function(){ $( "#progressbar" ).progressbar( "option", "value", ' . $progress . ' );});', CClientScript::POS_END); ?>

    $(document).on('click', '.gallery-item', function () {
        var answerId = $(this).data('answer-id');

        $('#UserSessionAnswer_answer_id').val(answerId).closest('form').submit();
    });
</script>