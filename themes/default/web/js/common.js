
/*
  modal windows Initialization
 */
var Carousel, Modal, Sharer,
  slice = [].slice;

Modal = (function() {
  var eventHide, eventShow, getModalId, hideModal, initScrollBar, loadContent, removeHeight, setHeight, showModal, updateHeight;

  function Modal(el) {
    this.element = el;
  }

  Modal.prototype.init = function() {
    var el, popup;
    el = this.element;
    popup = getModalId(el);
    eventShow(el, popup);
    return eventHide(popup);
  };

  Modal.prototype.show = function() {
    var popup;
    popup = getModalId(el);
    return showModal(popup);
  };

  Modal.prototype.hide = function() {
    var popup;
    popup = getModalId(el);
    return hideModal(this.element, popup);
  };

  getModalId = function(el) {
    var id;
    return id = $("#" + $(el).data('modal'));
  };

  showModal = function(d, el) {
    var data, p;
    $('html').addClass('locked');
    data = $(d).data();
    p = el.find('.ctx');
    if (data.getData != null) {
      loadContent(el, data.getData, p);
    }
    return el.addClass('show');
  };

  hideModal = function(el) {
    $('html').removeClass('locked');
    return el.removeClass('show');
  };

  eventShow = function(el, popup) {
    return $(el).on('click', function() {
      showModal(el, popup);
      return setHeight(popup);
    });
  };

  eventHide = function(popup) {
    return $(document).on('click', '.close, .md-overlay', function(e) {
      e.preventDefault();
      hideModal(popup);
      return removeHeight(popup);
    });
  };

  setHeight = function(popup) {
    return popup.css('height', $(window).height() + 'px');
  };

  removeHeight = function(popup) {
    return popup.css('height', '');
  };

  updateHeight = function(popup) {
    return $(window).on('resize', function() {
      return setHeight(popup);
    });
  };

  initScrollBar = function(popup) {
    setHeight(popup);
    updateHeight(popup);
    if (typeof $.fn.mCustomScrollbar !== 'undefined') {
      return popup.find('.content').mCustomScrollbar();
    }
  };

  loadContent = function(el, path, to) {
    return $.get(path, function(data) {
      to.html(data);
      return initScrollBar(el);
    });
  };

  return Modal;

})();


/*
  Carousels initialization
  Setup custom settings
  owl.carousels
 */

Carousel = (function() {
  var buildCarousel, eventsControls, fireShowingControls, jumpCarousel, jumpToCarousel, prepareResponseOption, resizeWindow, slideToCarousel, updateOptions;

  function Carousel(el) {
    this.element = el;
    this.defaults = {
      items: 1,
      loop: true,
      nav: false,
      mouseDrag: false,
      autoHeight: true,
      responsive: {}
    };
  }

  Carousel.prototype.init = function() {
    var def;
    def = this.defaults;
    buildCarousel(this.element, def);
    eventsControls(this.element, def);
    fireShowingControls(this.element, def);
    return resizeWindow(this.element);
  };

  Carousel.prototype.jump = function(to) {
    return jumpToCarousel(this.element, to);
  };

  Carousel.prototype.slideTo = function() {
    var opt;
    opt = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    return slideToCarousel.apply(null, opt);
  };

  prepareResponseOption = function(opts) {
    var key, value;
    for (key in opts) {
      value = opts[key];
      opts[key] = {
        items: value
      };
    }
    return opts;
  };

  updateOptions = function(el, def) {
    var opts, pluginOptions, responsiveOptions;
    pluginOptions = $(el).data('plugin-options');
    responsiveOptions = $(el).data('responsive');
    if ((pluginOptions != null) && Object.keys(pluginOptions).length !== 0) {
      $.extend(def, pluginOptions);
    }
    if ((responsiveOptions != null) && Object.keys(responsiveOptions).length !== 0) {
      opts = prepareResponseOption(responsiveOptions);
      $.extend(true, def.responsive, opts);
    }
    return def;
  };

  jumpCarousel = function(opt, to, speed) {
    var data;
    if (typeof $.fn.owlCarousel !== 'undefined') {
      speed = speed || 1;
      data = $(opt).owlCarousel();
      return data.trigger('to.owl.carousel', [to, speed]);
    }
  };

  buildCarousel = function(el, def) {
    var o, owl;
    if (typeof $.fn.owlCarousel !== 'undefined') {
      o = updateOptions(el, def);
      return owl = $(el).owlCarousel(o);
    }
  };

  slideToCarousel = function(el, direction) {
    var owl;
    if (typeof $.fn.owlCarousel !== 'undefined') {
      owl = $(el).owlCarousel();
      return owl.trigger(direction + '.owl.carousel');
    }
  };

  jumpToCarousel = function(el, number) {
    var owl;
    if (typeof $.fn.owlCarousel !== 'undefined') {
      owl = $(el).owlCarousel();
      return owl.trigger('to.owl.carousel', number);
    }
  };

  eventsControls = function(el) {
    var ctrl;
    ctrl = $(el).parent().find('[data-direction]');
    return ctrl.on('click', function() {
      return slideToCarousel(el, $(this).data().direction);
    });
  };

  fireShowingControls = function(el) {
    var getVisItems, item, owl;
    item = $(el).find('.owl-item').length;
    owl = el.owlCarousel().data('owlCarousel');
    getVisItems = owl.settings.items;
    if (item < getVisItems) {
      return $(el).parent().find('[data-direction]').hide();
    }
  };

  resizeWindow = function(el) {
    return $(window).on('resize', function() {
      return fireShowingControls(el);
    });
  };

  return Carousel;

})();


/*
  Share
 */

Sharer = (function() {
  var eventClick, getData, shareProviders;

  function Sharer(options) {
    this.options = options;
  }

  shareProviders = {
    facebook: function(data) {
      var url;
      url = 'http://www.facebook.com/sharer.php?s=100';
      url += '&p[url]=' + encodeURIComponent(data.url);
      return this.popup(url);
    },
    odnoklassniki: function(data) {
      var url;
      url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + encodeURIComponent(data.url);
      return this.popup(url);
    },
    twitter: function(data) {
      var url;
      url = 'http://twitter.com/share?';
      url += 'text=' + encodeURIComponent(data.title);
      url += '&url=' + encodeURIComponent(window.location);
      return this.popup(url);
    },
    popup: function(url) {
      return window.open(url, '', 'toolbar=0,status=0,width=554,height=436');
    }
  };

  Sharer.prototype.init = function() {
    var el, o;
    o = this.options;
    el = o.element;
    return eventClick(o, el);
  };

  getData = function(el, image) {
    var data, description, path, title;
    path = window.location;
    image = image;
    title = document.title;
    description = $('[name="description"]').attr('content');
    return data = {
      image: image,
      title: title,
      url: path + 'share.php?image=' + encodeURIComponent(image) + '&title=' + encodeURIComponent(title) + '&description=' + encodeURIComponent(description) + '&redirect=' + encodeURIComponent(path)
    };
  };

  eventClick = function(opt, el) {
    var link;
    link = $(opt.element).find(opt.shareButtons).find('[data-provider]');
    return link.on('click', function(e) {
      var data, image, type;
      image = $(this).data('share-image');
      data = getData(el, image);
      e.preventDefault();
      type = $(this).data('provider');
      return shareProviders[type](data);
    });
  };

  return Sharer;

})();


/*
  DOM ready
 */

$(function() {
  var isAndroid, modal, popupCarousel, share, thumbsCarousel, ua;
  $('.carousel').each(function() {
    var carousel;
    carousel = new Carousel($(this));
    return carousel.init();
  });
  popupCarousel = new Carousel($('.popup-carousel'));
  popupCarousel.init();
  thumbsCarousel = new Carousel($('.thumbs-carousel'));
  thumbsCarousel.init();

  /*
    Init popup slider
   */
  $('[data-card] img').on('click', function() {
    var target;
    target = $(this).parent().data('card');
    popupCarousel.jump(target);
    thumbsCarousel.jump(target);
    return $('.popup-slider').addClass('show');
  });
  $('.js-show-popup-slider').on('click', function(e) {
    e.preventDefault();
    return $('.popup-slider').addClass('show');
  });
  $('.popup-slider-overlay').on('click', function(e) {
    return $('.popup-slider').removeClass('show');
  });
  $('.popup-slider').on('click', function(e) {
    if (!$(e.target).is('.popup-slider *')) {
      return $('.popup-slider').removeClass('show');
    }
  });
  $('.thumbs-carousel').on('click', '.item', function() {
    var target;
    target = $(this).data('thumb-item');
    return popupCarousel.jump(target);
  });
  $('.popup-carousel').each(function() {
    var owl;
    owl = $(this).owlCarousel().data('owlCarousel');
    console.log(owl);
    return $(this).on('initialized.owl.carousel change.owl.carousel changed.owl.carousel', function(e) {
      var current, image, items;
      if (!e.namespace || e.type !== 'initialized' && e.property.name !== 'position') {
        return false;
      } else {
        current = e.relatedTarget.current();
        items = $(this).find('.owl-stage').children();
        image = items.eq(e.relatedTarget.normalize(current)).find('img').attr('src');
        $('[data-provider]').each(function() {
          return $(this).attr('data-share-image', image);
        });
        return $('.popup-slider .share-links').eq(e.relatedTarget.normalize(current) - 1).removeClass('hide').addClass('visible').siblings('.share-links').removeClass('visible').addClass('hide');
      }
    });
  });

  /*
    Initialize sharing
   */
  share = new Sharer({
    element: '.popup-slider .big-images',
    shareButtons: '.share-links'
  }).init();

  /*
    inti modals
   */
  modal = new Modal('.js-modal-show');
  modal.init();

  /*
    Fire mobile os
   */
  ua = navigator.userAgent.toLowerCase();
  isAndroid = ua.indexOf("android") > -1;
  if (isAndroid) {
    $('.store.google').show();
    $('.store.tunec').hide();
  }

  /*
    init parallax
   */
  return $.stellar({
    positionProperty: 'transform'
  });
});

//# sourceMappingURL=common.js.map