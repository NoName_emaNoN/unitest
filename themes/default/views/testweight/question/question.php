<?php
/**
 * @var $this QuestionController
 * @var $question Question
 * @var $session UserSession
 * @var $answer UserSessionAnswer
 * @var $form CActiveForm
 * @var $test Test
 */

$current = $session->answerCount + 1;
$total = Question::model()->active()->test($test->id)->count();
$progress = round($current / $total * 100);
?>



<section class="test-section section-5">
    <a name="test"></a>

    <div class="container">
        <div class="section-intro text-center">
            <h2 class="section-title">Весна – пора влюбляться!</h2>

            <p>Пройдите тест и найдите своего сказочного принца! </p>
        </div>
        <div class="module-test">
            <div class="test-title"><?= $question->text ?><span class="num"><?= $current ?></span></div>
            <div class="test-image">
                <?php if ($question->image): ?>
                    <figure class="image"><?= CHtml::image($question->getImageUrl(), '', ['class' => 'img-responsive']) ?></figure>
                <?php endif; ?>
            </div>
            <div class="test-questions">
                <?php
                $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id'                     => 'test-form',
                        'method'                 => 'POST',
                        'enableClientValidation' => true,
                    )
                ); ?>
                <?= $form->hiddenField($answer, 'question_id'); ?>

                <?= $form->radioButtonList(
                    $answer,
                    'answer_id',
                    CHtml::listData($question->answers, 'id', 'text'),
                    array(
                        'template'  => '<div class="item item-1">
                    {beginLabel}
                        {input}<span class="overlay"></span><span class="inner-text">{labelTitle}</span>
                    {endLabel}
                </div>',
                        'disabled'  => !$answer->isNewRecord,
                        'separator' => false
                    )
                ); ?>

                <?php $this->endWidget(); ?>
            </div>
            <div class="test-progress-row">
                <div class="count pull-right"><?= $current ?> из <?= $total ?></div>
                <div class="test-progress-bar">
                    <div style="width:<?= $progress ?>%" class="pr-el"></div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $('#UserSessionAnswer_answer_id').on('change', function () {
        $('#test-form').submit();
    });
</script>