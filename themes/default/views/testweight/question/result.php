<?php
/**
 * @var $this QuestionController
 * @var $session UserSession
 */

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
$outcome = $session->getOutcome();

Yii::app()->clientScript->registerMetaTag($outcome->name, null, null, ['property' => 'og:title']);
Yii::app()->clientScript->registerMetaTag($session->getUrl(true), null, null, ['property' => 'og:url']);
Yii::app()->clientScript->registerMetaTag($outcome->getImageUrl(), null, null, ['property' => 'og:image']);
Yii::app()->clientScript->registerMetaTag($outcome->text, null, null, ['property' => 'og:description']);
Yii::app()->clientScript->registerMetaTag('ru_RU', null, null, ['property' => 'og:locale']);
?>

<section class="test-section section-5">
    <a name="test"></a>

    <div class="container">
        <div class="section-intro text-center">
            <h2 class="section-title">Весна – пора влюбляться!</h2>

            <p>Пройдите тест и найдите своего сказочного принца! </p>
        </div>
        <div class="module-test">
            <div class="test-image">
                <?php if ($outcome->image): ?>
                    <figure class="image"><?= CHtml::image($outcome->getImageUrl(), $outcome->name, ['class' => 'img-responsive']); ?></figure>
                <?php endif; ?>
            </div>
            <div class="result-test">
                <div class="inner">
                    <h2 class="title"><?= $outcome->name ?></h2>

                    <p><?= $outcome->text ?></p>
                </div>
                <div class="share-links">

                    <?= CHtml::link('<span></span>', 'https://www.facebook.com/sharer/sharer.php?u=' . $session->getUrl(true), ['class' => 'share share-fb', 'target' => '_blank']); ?>
                    <!--                    --><? //= CHtml::link(
                    //                        '<span></span>',
                    //                        'https://pinterest.com/pin/create/button/?url=' . $session->getUrl(true) . '&media=' . $outcome->getImageUrl() . '&description=' . $outcome->name,
                    //                        ['class' => 'share share-odn', 'target' => '_blank']
                    //                    ); ?>
                    <?= CHtml::link(
                        '<span></span>',
                        'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' . $session->getUrl(true) . '&st.comments=' . $outcome->name,
                        ['class' => 'share share-odn', 'target' => '_blank']
                    ); ?>
                    <?= CHtml::link('<span></span>', 'https://twitter.com/intent/tweet?url=' . $session->getUrl(true) . '&text=' . $outcome->name, ['class' => 'share share-tw', 'target' => '_blank']); ?>

                </div>
                <div class="text-center"><br><?= CHtml::link('Пройти тест еще раз', ['start', 'test_id' => $session->test_id], ['class' => 'btn']); ?></div>
            </div>
        </div>
    </div>
</section>