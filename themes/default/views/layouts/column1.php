<?php $this->beginContent('//layouts/main'); ?>
<?php $mainAssets = Yii::app()->theme->getAssetsUrl(); ?>

<div class="global">
    <div class="content">
        <section class="section-1">
            <div class="container">
                <div class="top-text text-center">
                    <h1><img src="<?= $mainAssets ?>/images/top-text.png" alt="Радости весны" class="inline img-responsive"></h1>
                </div>
                <div class="get-concurs-section white-block">
                    <div data-card="0" class="intro-image"><img src="<?= $mainAssets ?>/assets/get-concurs/1.jpg" alt="Весна – повод делиться радостью!" class="img-responsive inline"></div>
                    <div class="intro-part text-center">
                        <h2 class="section-title">Весна – повод делиться радостью!</h2>

                        <p>У вас есть шанс выиграть волшебные призы от Disney! С 1 по 20 марта выберите одну из ярких открыток, расскажите, что для вас значит весна, и поделитесь своей работой в социальных сетях. </p><a href="#"
                                                                                                                                                                                                                            target="_blank"
                                                                                                                                                                                                                            class="btn js-show-popup-slider">Принять
                            участие</a>
                    </div>
                    <div class="items">
                        <div data-card="1" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/2.jpg" alt="" class="img-responsive full-width"><span id="rules-popap" data-modal="rules-popup"
                                                                                                                                                              data-get-data="<?= $mainAssets ?>/db/rules.html"
                                                                                                                                                              class="show-popup js-modal-show">Правила конкурса</span>
                        </div>
                        <div data-card="2" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/3.jpg" alt="" class="img-responsive full-width">
                        </div>
                        <div data-card="3" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/4.jpg" alt="" class="img-responsive full-width">
                        </div>
                        <div data-card="4" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/5.jpg" alt="" class="img-responsive full-width">
                        </div>
                        <div data-card="5" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/6.jpg" alt="" class="img-responsive full-width">
                        </div>
                        <div data-card="6" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/7.jpg" alt="" class="img-responsive full-width">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="baners text-center">
            <div class="container">
                <div class="baner"><a href="http://cinderellamovie.disney.ru/"><img src="<?= $mainAssets ?>/assets/baner-1.jpg" alt="" class="img-responsive full-width"></a></div>
                <div class="baner"><a href="http://beauty-musical.ru/"><img src="<?= $mainAssets ?>/assets/baner-2.jpg" alt="" class="img-responsive full-width"></a></div>
                <div class="baner"><a href="http://www.disney.ru/kanal/"><img src="<?= $mainAssets ?>/assets/baner-3.jpg" alt="" class="img-responsive full-width"></a></div>
            </div>
        </div>
        <section class="section-2">
            <div class="container">
                <div class="movies-section white-block">
                    <div class="text-center">
                        <h2 class="section-title">Весна – время волшебных историй!</h2>

                        <p>Мечтайте вместе с любимыми героями! Смотрите весеннюю коллекцию фильмов от Disney и Apple. </p>
                    </div>
                    <div class="items">
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/aristocats.jpg" alt="Коты Аристократы" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Коты Аристократы</div>
                                        <div class="overlay-eng-title">Aristocats</div>
                                        <div class="year"><span class="t">Год:</span><span>1970</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Коты_Аристократы?id=ouskD8v22Ng" title="Коты Аристократы" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/movie/koty-aristokraty/id187696408" title="Коты Аристократы" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Коты Аристократы</span></div>
                                <div class="time">75 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/beauty_and_the_beast.jpg" alt="Красавица и Чудовище" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Красавица и Чудовище</div>
                                        <div class="overlay-eng-title">Beauty and The Beast</div>
                                        <div class="year"><span class="t">Год:</span><span>2002</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Красавица_и_Чудовище?id=FZyhgBsu210" title="Красавица и Чудовище" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/movie/krasavica-i-cudovise/id607522458" title="Красавица и Чудовище" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Красавица и Чудовище</span></div>
                                <div class="time">81 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/cinderella.jpg" alt="Золушка" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Золушка</div>
                                        <div class="overlay-eng-title">Cinderella</div>
                                        <div class="year"><span class="t">Год:</span><span>2012</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Золушка?id=ApnHb4Jgl58" title="Золушка" target="_blank" class="store google"></a><a href="https://itunes.apple.com/ru/movie/zoluska/id615957180"
                                                                                                                                                                                  title="Золушка" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Золушка</span></div>
                                <div class="time">71 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/enchanted.jpg" alt="Зачарованная" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Зачарованная</div>
                                        <div class="overlay-eng-title">Enchanted</div>
                                        <div class="year"><span class="t">Год:</span><span>2007</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Зачарованная?id=8fRBcfYVMls" title="Зачарованная" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/movie/zacarovannaa/id619838710" title="Зачарованная" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Зачарованная</span></div>
                                <div class="time">103 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/lady_and_the_tramp.jpg" alt="Леди и Бродяга" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Леди и Бродяга</div>
                                        <div class="overlay-eng-title">Lady and the Tramp</div>
                                        <div class="year"><span class="t">Год:</span><span>2012</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Леди_и_Бродяга?id=dhuE31oJKQ0" title="Леди и Бродяга" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/movie/ledi-i-brodaga/id629080167" title="Леди и Бродяга" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Леди и Бродяга</span></div>
                                <div class="time">73 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/little_mermaid.jpg" alt="Русалочка" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Русалочка</div>
                                        <div class="overlay-eng-title">The Little Mermaid</div>
                                        <div class="year"><span class="t">Год:</span><span>1989</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Русалочка?id=qSWo5FFWmNo" title="Русалочка" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/movie/rusalocka/id676916840" title="Русалочка" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Русалочка</span></div>
                                <div class="time">79 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/princess_and_the_frog.jpg" alt="Принцесса и лягушка" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Принцесса и лягушка</div>
                                        <div class="overlay-eng-title">Princess and the Frog</div>
                                        <div class="year"><span class="t">Год:</span><span>2010</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Принцесса_и_лягушка?id=N6aXPqQe3Rs" title="Принцесса и лягушка" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/movie/princessa-i-laguska/id346327212" title="Принцесса и лягушка" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Принцесса и лягушка</span></div>
                                <div class="time">94 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/sleeping_beauty.jpg" alt="Спящая красавица" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Спящая красавица</div>
                                        <div class="overlay-eng-title">Sleeping Beauty</div>
                                        <div class="year"><span class="t">Год:</span><span>1959</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Спящая_красавица?id=TLFkYWh7Sew" title="Спящая красавица" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/movie/spasaa-krasavica/id927381854" title="Спящая красавица" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Спящая красавица</span></div>
                                <div class="time">72 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/tangled.jpg" alt="Рапунцель" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Рапунцель</div>
                                        <div class="overlay-eng-title">Tangled</div>
                                        <div class="year"><span class="t">Год:</span><span>2010</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Рапунцель_Запутанная_история?id=QzEPz3na3Yg" title="Рапунцель" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/movie/rapuncel-zaputannaa-istoria/id409247101" title="Рапунцель" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>Рапунцель</span></div>
                                <div class="time">96 мин.</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/films/wall-e.jpg" alt="ВАЛЛ-И" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">ВАЛЛ-И</div>
                                        <div class="overlay-eng-title">WALL-E</div>
                                        <div class="year"><span class="t">Год:</span><span>2008</span></div>
                                        <div class="old"><span class="t">Возраст: </span><span>0+</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/movies/details/Валл_И?id=oMmhxUFonyE" title="ВАЛЛ-И" target="_blank" class="store google"></a><a href="https://itunes.apple.com/ru/movie/vall-i/id286533539"
                                                                                                                                                                                title="ВАЛЛ-И" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title"><span>ВАЛЛ-И</span></div>
                                <div class="time">94 мин.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-3">
            <div class="container">
                <div class="paint-section">
                    <div class="text-center">
                        <h2 class="section-title">Весна – буйство красок!</h2>

                        <p>Сделайте весну яркой с этими раскрасками.</p>
                    </div>
                    <div class="carousel-wrapper">
                        <div class="nav">
                            <div data-direction="prev" class="cntrl prev"></div>
                            <div data-direction="next" class="cntrl next"></div>
                        </div>
                        <div data-plugin-options="{&quot;items&quot;:5, &quot;loop&quot;: true, &quot;nav&quot;:false, &quot;margin&quot;:20}"
                             data-responsive="{&quot;0&quot;:1, &quot;340&quot;: 2, &quot;478&quot;: 2, &quot;768&quot;: 3, &quot;992&quot;: 4, &quot;1063&quot;: 5}" class="items paint-carousel carousel">
                            <div data-item="0" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_1.pdf" title="bambi coloring 1" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/1.jpg" alt="bambi coloring 1"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="1" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_3.pdf" title="bambi coloring 3" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/2.jpg" alt="bambi coloring 3"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="2" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_3.pdf" title="bambi coloring 3" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/3.jpg" alt="bambi coloring 3"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="3" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_4.pdf" title="bambi coloring 4" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/4.jpg" alt="bambi coloring 4"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="4" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_5.pdf" title="bambi coloring 5" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/5.jpg" alt="bambi coloring 5"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="5" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_6.pdf" title="bambi coloring 6" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/6.jpg" alt="bambi coloring 6"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="6" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_7.pdf" title="bambi coloring 7" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/7.jpg" alt="bambi coloring 7"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="7" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_8.pdf" title="bambi coloring 8" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/8.jpg" alt="bambi coloring 8"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="8" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_9.pdf" title="bambi coloring 9" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/9.jpg" alt="bambi coloring 9"
                                                                                                                                                                 class="img-responsive full-width"></a></figure>
                            </div>
                            <div data-item="9" class="item">
                                <figure class="image"><a href="<?= $mainAssets ?>/assets/PDF/bambi_coloring_10.pdf" title="bambi coloring 10" target="_blank"><img src="<?= $mainAssets ?>/assets/paints/10.jpg" alt="bambi coloring 10"
                                                                                                                                                                   class="img-responsive full-width"></a>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-4">
            <div class="container">
                <div class="games-section white-block">
                    <div class="text-center">
                        <h2 class="section-title">Весна – сезон новых побед!</h2>

                        <p>Поднимите себе настроение с лучшими играми от Disney в вашем мобильном. </p>
                    </div>
                    <div class="items">
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/games/1.jpg" alt="Холодное сердце. Звездопад" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Холодное сердце. Звездопад</div>
                                        <div class="overlay-catgory"><span class="t">Категория: </span><span>Пазлы</span></div>
                                        <div class="stars stars-1"><span class="color"></span><span></span><span></span><span></span><span></span>
                                        </div>
                                        <a href="https://control.kochava.com/v1/cpi/click?campaign_id=kofrozenfreefallandroid129852658c49978953faccdda7fb55&amp;network_id=1668&amp;device_id=device_id&amp;site_id=RU-web-DISNEY-Card-201502&amp;append_app_conv_trk_params=1"
                                           title="Холодное сердце. Звездопад" target="_blank" class="store google"></a><a
                                            href="https://control.kochava.com/v1/cpi/click?campaign_id=kofrozenfreefallios129752658bedb244dd722280263bc7&amp;network_id=1668&amp;device_id=device_id&amp;site_id=1"
                                            title="Холодное сердце. Звездопад" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title">Холодное сердце. Звездопад</div>
                                <div class="catgory">Пазлы</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/games/2.jpg" alt="Где же валентинка?" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Где же валентинка?</div>
                                        <div class="overlay-catgory"><span class="t">Категория: </span><span>Другое</span></div>
                                        <div class="stars stars-2"><span></span><span class="color"></span><span></span><span></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/apps/details?id=com.disney.WheresMyHoliday_GOO" title="Где же валентинка?" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/app/gde-ze-valentinka/id575697157?mt=8" title="Где же валентинка?" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title">Где же валентинка?</div>
                                <div class="catgory">Другое</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/games/3.jpg" alt="Малефисента Звездопад" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Малефисента Звездопад</div>
                                        <div class="overlay-catgory"><span class="t">Категория: </span><span>Пазлы</span></div>
                                        <div class="stars stars-3"><span></span><span></span><span class="color"></span><span></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/apps/details?id=com.disney.maleficent_goo" title="Малефисента Звездопад" target="_blank" class="store google"></a><a
                                            href="https://control.kochava.com/v1/cpi/click?campaign_id=komaleficentfreefallios3480533b2a3d68119a0b187fb55abe&amp;network_id=1668&amp;device_id=device_id&amp;site_id=1"
                                            title="Малефисента Звездопад" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title">Малефисента Звездопад</div>
                                <div class="catgory">Пазлы</div>
                            </div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/games/4.jpg" alt="Королевские питомцы" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Королевские питомцы</div>
                                        <div class="overlay-catgory"><span class="t">Категория: </span><span>Развлечения</span></div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://itunes.apple.com/ru/app/korolevskie-pitomcy/id643052753?mt=8" title="Королевские питомцы" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="data">
                                <div class="title">Королевские питомцы</div>
                                <div class="catgory">Развлечения</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php echo $content; ?>

        <section class="section-6">
            <div class="container">
                <div class="music-section white-block darken">
                    <div class="text-center">
                        <h2 class="section-title">Весна – время музыки и танцев!</h2>
                    </div>
                    <div class="items text-center">
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/1.png" alt="Виолетта - Hoy somos mas" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Виолетта - Hoy somos mas</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/music/album/Various_Artists_Violetta_Hoy_somos_mas?id=B4dvwwsri32nhrfjd6hltrgojdq" title="Виолетта - Hoy somos mas" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/album/violetta-hoy-somos-mas/id852292257" title="Виолетта - Hoy somos mas" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Виолетта - Hoy somos mas</div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/2.png" alt="Малефисента" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Малефисента</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/music/album/Maleficent_Maleficent_Original_Motion_Picture_Soun?id=Bokwoayqdtxduyqhzz2j3vvxj2e" title="Малефисента" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/album/maleficent-original-motion/id867866225" title="Малефисента" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Малефисента</div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/3.png" alt="Принцесса и лягушка" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Принцесса и лягушка</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="http://goo.gl/XVaqK4" title="Принцесса и лягушка" target="_blank" class="store google"></a><a href="https://itunes.apple.com/ru/album/princessa-i-laguska/id852194732"
                                                                                                                                               title="Принцесса и лягушка" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Принцесса и лягушка</div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/4.png" alt="Disney Jazz Volume I: Everybody Wants To Be A Cat" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Disney Jazz Volume I: Everybody Wants To Be A Cat</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/music/album/Various_Artists_Disney_Jazz_Volume_I_Everybody_Wan?id=Brq73yxa" title="Disney Jazz Volume I: Everybody Wants To Be A Cat" target="_blank"
                                           class="store google"></a><a href="https://itunes.apple.com/ru/album/disney-jazzvol.-i-everybody/id875197275" title="Disney Jazz Volume I: Everybody Wants To Be A Cat" target="_blank"
                                                                       class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Disney Jazz Volume I: Everybody Wants To Be A Cat</div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/5.png" alt="Рапунцель: Запутанная история " class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Рапунцель: Запутанная история</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="http://goo.gl/jChDxw" title="Рапунцель: Запутанная история " target="_blank" class="store google"></a><a href="https://itunes.apple.com/ru/album/rapuncel-zaputannaa-istoria/id914660513"
                                                                                                                                                          title="Рапунцель: Запутанная история " target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Рапунцель: Запутанная история</div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/6.png" alt="Disneymania 5" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Disneymania 5</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/music/album/Various_Artists_Disneymania_5?id=Bwyk5kunwdfhi5nn7jkcboudyma" title="Disneymania 5" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/album/disneymania-5/id924455937" title="Disneymania 5" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Disneymania 5</div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/7.png" alt="Холодное сердце" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Холодное сердце</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="http://goo.gl/OJCqoH" title="Холодное сердце" target="_blank" class="store google"></a><a href="http://smarturl.it/OSTFrozen" title="Холодное сердце" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Холодное сердце</div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/8.png" alt="Disney Pixar. Лучшее" class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Disney Pixar. Лучшее</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://play.google.com/store/music/album/Various_Artists_Disney_Pixar_Greatest?id=Bsrjxsbytpwt4jv6zgyadbu4hmi" title="Disney Pixar. Лучшее" target="_blank" class="store google"></a><a
                                            href="https://itunes.apple.com/ru/album/disney-pixar-greatest/id847388387" title="Disney Pixar. Лучшее" target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Disney Pixar. Лучшее</div>
                        </div>
                        <div class="item overlay-item">
                            <figure class="image"><img src="<?= $mainAssets ?>/assets/music/9.png" alt="Вверх " class="img-responsive full-width">

                                <div class="overlay">
                                    <div class="inner">
                                        <div class="overlay-title">Вверх</div>
                                        <div class="stars stars-4"><span></span><span></span><span></span><span class="color"></span><span></span>
                                        </div>
                                        <a href="https://itunes.apple.com/ru/album/up-original-motion-picture/id893787382" title="Вверх " target="_blank" class="store tunec"></a>
                                    </div>
                                </div>
                            </figure>
                            <div class="title">Вверх</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <footer class="footer">
        <div class="container text-center"><a href="http://disney.ru" target="_blank" class="dsn-foo"><img src="<?= $mainAssets ?>/images/dsn-logo.png" alt="Disney!" style="width:72px"></a>
            <nav class="foo-nav">
                <ul>
                    <li><a href="http://www.waltdisney.ru/" target="_blank">Корпоративная информация</a></li>
                    <li><a href="http://www.disney.ru/safesurfing/" target="_blank">Безопасный интернет</a></li>
                    <li><a href="https://key.disney.ru/signup/terms" target="_blank">Условия использования</a></li>
                    <li><a href="https://key.disney.ru/signup/terms" target="_blank">Защита данных</a></li>
                    <li><a href="http://www.disney.ru/adsales/" target="_blank">Реклама</a></li>
                </ul>
            </nav>
            <p>© Disney. Все права защищены. </p>

            <p>0+ Информационная продукция, опубликованная на данном сайте, предназначена для любой аудитории, если иное не указано дополнительно в отношении отдельных материалов</p>

            <div class="socials text-center"><a href="http://www.disney.ru/redirect.jsp?to=http://www.facebook.com/campaign/landing.php?campaign_id=199265800115369" target="_blank" title="Мы в Facebook" class="socials-link fb"> </a><a
                    href="http://www.disney.ru/redirect.jsp?to=http://twitter.com/DisneyRussia" target="_blank" title="Мы в Twitter" class="socials-link tw"> </a><a
                    href="http://www.disney.ru/redirect.jsp?to=http://www.odnoklassniki.ru/Disney" target="_blank" title="Мы в Одноклассниках" class="socials-link odn"> </a><a
                    href="http://www.disney.ru/redirect.jsp?to=http://www.youtube.com/waltdisneystudiosru" target="_blank" title="Мы в Youtube" class="socials-link ytb"> </a><a
                    href="http://www.disney.ru/redirect.jsp?to=http://Instagram.com/DisneyRussia" target="_blank" title="Мы в Instagram" class="socials-link inst"> </a>
            </div>
        </div>
    </footer>
    <div id="rules-popup" class="modal">
        <div class="bg-m">
            <div class="close"></div>
            <div class="content">
                <div class="ctx"></div>
            </div>
        </div>
    </div>
    <div class="md-overlay"></div>
    <div class="popup-slider">
        <div class="big-images">
            <div data-plugin-options="{&quot;items&quot;: 1, &quot;mouseDrag&quot;: true}" class="items popup-carousel carousel">
                <div data-big-item="0" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/bigs/1.jpg" alt="" class="img-responsive full-width"></div>
                <div data-big-item="1" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/bigs/2.jpg" alt="" class="img-responsive full-width"></div>
                <div data-big-item="2" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/bigs/3.jpg" alt="" class="img-responsive full-width"></div>
                <div data-big-item="3" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/bigs/4.jpg" alt="" class="img-responsive full-width"></div>
                <div data-big-item="4" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/bigs/5.jpg" alt="" class="img-responsive full-width"></div>
                <div data-big-item="5" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/bigs/6.jpg" alt="" class="img-responsive full-width"></div>
                <div data-big-item="6" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/bigs/7.jpg" alt="" class="img-responsive full-width"></div>
            </div>
            <div class="share-links visible"><a href="#" target="_blank" data-provider="facebook" class="share share-fb"><span></span></a><a href="#" target="_blank" data-provider="odnoklassniki" class="share share-odn"><span></span></a><a
                    href="#" target="_blank" data-provider="twitter" class="share share-tw"><span></span></a></div>
            <div class="share-links hide"><a href="#" target="_blank" data-provider="facebook" class="share share-fb"><span></span></a><a href="#" target="_blank" data-provider="odnoklassniki" class="share share-odn"><span></span></a><a
                    href="#" target="_blank" data-provider="twitter" class="share share-tw"><span>								</span></a></div>
            <div class="share-links hide"><a href="#" target="_blank" data-provider="facebook" class="share share-fb"><span></span></a><a href="#" target="_blank" data-provider="odnoklassniki" class="share share-odn"><span></span></a><a
                    href="#" target="_blank" data-provider="twitter" class="share share-tw"><span>								</span></a></div>
            <div class="share-links hide"><a href="#" target="_blank" data-provider="facebook" class="share share-fb"><span></span></a><a href="#" target="_blank" data-provider="odnoklassniki" class="share share-odn"><span></span></a><a
                    href="#" target="_blank" data-provider="twitter" class="share share-tw"><span>								</span></a></div>
            <div class="share-links hide"><a href="#" target="_blank" data-provider="facebook" class="share share-fb"><span></span></a><a href="#" target="_blank" data-provider="odnoklassniki" class="share share-odn"><span></span></a><a
                    href="#" target="_blank" data-provider="twitter" class="share share-tw"><span>								</span></a></div>
            <div class="share-links hide"><a href="#" target="_blank" data-provider="facebook" class="share share-fb"><span></span></a><a href="#" target="_blank" data-provider="odnoklassniki" class="share share-odn"><span></span></a><a
                    href="#" target="_blank" data-provider="twitter" class="share share-tw"><span>								</span></a></div>
            <div class="share-links hide"><a href="#" target="_blank" data-provider="facebook" class="share share-fb"><span></span></a><a href="#" target="_blank" data-provider="odnoklassniki" class="share share-odn"><span></span></a><a
                    href="#" target="_blank" data-provider="twitter" class="share share-tw"><span>								</span></a></div>
        </div>
        <div class="carousel-wrapper">
            <div class="nav">
                <div data-direction="prev" class="cntrl prev"></div>
                <div data-direction="next" class="cntrl next"></div>
            </div>
            <div data-plugin-options="{&quot;items&quot;: 5, &quot;mouseDrag&quot;: true, &quot;autoHeight &quot;: true, &quot;margin&quot;: 40, &quot;loop&quot;:false}"
                 data-responsive="{&quot;0&quot;:1, &quot;340&quot;: 2, &quot;478&quot;: 3, &quot;768&quot;: 4, &quot;992&quot;: 5, &quot;1063&quot;: 5}" class="items thumbs-carousel carousel">
                <div data-thumb-item="0" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/1.jpg" alt="" class="img-responsive full-width"></div>
                <div data-thumb-item="1" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/2.jpg" alt="" class="img-responsive full-width"></div>
                <div data-thumb-item="2" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/3.jpg" alt="" class="img-responsive full-width"></div>
                <div data-thumb-item="3" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/4.jpg" alt="" class="img-responsive full-width"></div>
                <div data-thumb-item="4" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/5.jpg" alt="" class="img-responsive full-width"></div>
                <div data-thumb-item="5" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/6.jpg" alt="" class="img-responsive full-width"></div>
                <div data-thumb-item="6" class="item"><img src="<?= $mainAssets ?>/assets/get-concurs/7.jpg" alt="" class="img-responsive full-width"></div>
            </div>
        </div>
    </div>
    <div class="popup-slider-overlay"></div>
    <div class="parallax">
        <div data-stellar-ratio="0.02" class="parallax-1"></div>
        <div data-stellar-ratio=".15" class="parallax-2"></div>
        <div data-stellar-ratio=".35" class="parallax-3"></div>
        <div class="parallax-4"></div>
    </div>
</div>
<?php $this->endContent(); ?>
