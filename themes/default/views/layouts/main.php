<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="<?php echo Yii::app()->charset; ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();

    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/libs.css');
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/style.css');
    Yii::app()->getClientScript()->registerCssFile('http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&amp;subset=latin,cyrillic');

    Yii::app()->getClientScript()->registerPackage('jquery');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/libs.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/common.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/globalhrome.js', CClientScript::POS_END);
    ?>

    <!--HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries--><!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script><![endif]-->

</head>
<body>

<?= $content; ?>
<!-- flashMessages -->
<!--    --><?php //$this->widget('yupe\widgets\YFlashMessages'); ?>

<?php if (Yii::app()->hasModule('contentblock')): ?>
    <?php $this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        array("code" => "STAT", "silent" => true)
    ); ?>
<?php endif; ?>

</body>
</html>